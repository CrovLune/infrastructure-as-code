# IP2
## Game Service
This Service does all the game logic.

### Project setup
```
./gradlew build
```

### Compiles and hot-reloads for development
```
./gradlew run
```

### API Documentation
```
http://localhost:9090/swagger-ui/index.html
```

### Customize configuration
See [Configuration Reference](https://spring.io/projects/spring-boot).
