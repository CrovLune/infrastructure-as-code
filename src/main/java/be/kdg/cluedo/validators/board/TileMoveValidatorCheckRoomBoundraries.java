package be.kdg.cluedo.validators.board;

import be.kdg.cluedo.domain.tiles.Tile;
import be.kdg.cluedo.domain.tiles.TileType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TileMoveValidatorCheckRoomBoundraries implements TileMoveValidator{

    public boolean isTileValid(Tile from, Tile to) {
        return (from.getType() == TileType.DOOR || from.getType() == TileType.PORTAL) || to.getArea() == from.getArea() && to.getType() == TileType.WALKABLE;
    }
}
