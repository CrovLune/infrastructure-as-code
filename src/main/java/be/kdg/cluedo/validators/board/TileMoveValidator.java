package be.kdg.cluedo.validators.board;

import be.kdg.cluedo.domain.tiles.Tile;

public interface TileMoveValidator {
    boolean isTileValid(Tile from, Tile to);
}
