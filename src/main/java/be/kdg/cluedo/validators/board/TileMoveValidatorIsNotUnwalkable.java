package be.kdg.cluedo.validators.board;

import be.kdg.cluedo.domain.tiles.Tile;
import be.kdg.cluedo.domain.tiles.TileType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TileMoveValidatorIsNotUnwalkable implements TileMoveValidator {

    public boolean isTileValid(Tile from, Tile to) {
        return (to.getType() != TileType.UNWALKABLE || to.getArea() == from.getArea());
    }
}
