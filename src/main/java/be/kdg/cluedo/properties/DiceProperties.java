package be.kdg.cluedo.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "dice")
@PropertySource("classpath:dice.properties")
public class DiceProperties {
    private int maxSides;
    private int total;
}
