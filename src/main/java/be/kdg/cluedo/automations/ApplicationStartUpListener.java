package be.kdg.cluedo.automations;

import be.kdg.cluedo.domain.Envelope;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.factories.Factory;
import be.kdg.cluedo.services.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("ALL")
@Component
@Order(0)
@Slf4j
@AllArgsConstructor
@Transactional
public class ApplicationStartUpListener implements ApplicationListener<ApplicationReadyEvent> {
    private final BoardService boardService;
    private final GameService gameService;
    private final Factory factory;
    private final StartingPositionService startingPositionService;
    private final CardService cardService;
    private final EnvelopeService envelopeService;
    private final PlayerService playerService;
    private final Boolean createDummyGame = true;

    /**
     * This method listens to Application-Ready call, when SpringBoot is fully initiated then the Database
     * is being populated with dummy data.
     * <p>
     * Creates:
     * - Board
     * - Starting Pos for each Character (Board)
     * - Game (+links to Board)
     * - Cards (+link to Board)
     * - Envelope (+link to Game)
     * - Puts Cards in Envelope + puts rest of the Cards in Game
     * <p>
     * Doesn't Create:
     * - Players (+link to Game & Cards)
     *
     * @Author : Zelenskiy Aleksey
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        //TODO: Add more checks
        if (boardService.getAllBoards().size() == 0) {
            log.info("Starting Automated Data Creation...");

            //? Create Board
            var standardBoard = factory.createBoard("StandardBoard", "standardMap.json");

            //? Save Board
            this.boardService.save(standardBoard);
            log.info("StandardBoard is saved in DATABASE");

            //? Create & Save Character Positions
            this.startingPositionService.setStartingPositions(standardBoard);
            log.info("Positions added");

            //? Associate Cards with Board
            this.cardService.addBoardToCards(standardBoard);
            log.info("Cards Linked");

            //? ##### Dummy Game Creation ######
            if (this.createDummyGame && this.gameService.getAllGames().size() == 0) {
                //? Create & Save Game
                var game = new Game(standardBoard);
                this.gameService.save(game);
                log.info("Game Created");

                //? Create & Save Envelope
                var env = new Envelope(game);
                this.envelopeService.save(env);

                //? Put Cards in Game
                this.cardService.putCardsInGame(game);
                log.info("Cards Added to Game");

                //? Creates dummy players
                for (int i = 0; i < CharacterType.values().length; i++) {
                    var player = new Player("Player " + i, CharacterType.values()[i], game);
                    this.playerService.save(player);
                }
                this.cardService.spreadCardsOverPlayers(game);
                this.playerService.setCurrentPlayer(game);
                this.playerService.setStartPosition(game);

                log.info(String.format("Dummy Game + Players Created GameID[%d]", game.getId()));
            }
            log.info("Ending Automated Data Creation");
        } else {
            log.info("Automations Skipped...");
        }
    }
}
