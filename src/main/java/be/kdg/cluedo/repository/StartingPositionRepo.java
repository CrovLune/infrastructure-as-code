package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.factories.StartingPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StartingPositionRepo extends JpaRepository<StartingPosition, Long> {

//    StartingPosition readStartingPositionByCharacterName(String name);
    StartingPosition readStartingPositionByBoardAndCharacterName(Board board, String name);
}
