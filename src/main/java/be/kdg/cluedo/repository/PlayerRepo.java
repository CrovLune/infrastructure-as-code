package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.players.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepo extends JpaRepository<Player, Long> {

    Player readPlayerById(Long id);

    List<Player> readPlayersByGame(Game game);

    List<Player> readPlayersByGame_Id(Long gameId);
}
