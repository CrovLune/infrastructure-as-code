package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoardRepo extends JpaRepository<Board,Long> {
    Board readBoardByName(String name);
    Board readBoardById(long boardId);
    Board readBoardByAreasIsContaining(Area area);
}
