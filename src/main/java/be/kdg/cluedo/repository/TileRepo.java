package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.tiles.Tile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TileRepo extends JpaRepository<Tile, Long> {
    //TODO: change board to boardID
    //* looks for a <Tile> in a <Board> by <x> and <y> coordinates.
    Tile readTileByArea_BoardAndPosition_xAndPosition_y(Board board, int x, int y);

    //* looks for List<Tile> in a <Board> by <room name>.
    List<Tile> readTilesByArea_BoardAndArea_Name(Board board, String roomName);

    //* looks for List<Tile> in a <Board>.
    List<Tile> readTilesByArea_Board(Board board);

    Tile readTilesByArea_BoardAndId(Board board, Long tileId);
}
