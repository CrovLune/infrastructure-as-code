package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Envelope;
import be.kdg.cluedo.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvelopeRepo extends JpaRepository<Envelope, Long> {
    Envelope readEnvelopeByGame(Game game);
    Envelope readEnvelopeByGame_Id(Long gameId);
}
