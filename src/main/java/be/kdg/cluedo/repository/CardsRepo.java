package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.cards.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepo extends JpaRepository<Card, Long> {

    List<Card> readCardsByBoardsContainingAndType(Board board, String type);

    List<Card> readCardsByBoardsContaining(Board board);
}
