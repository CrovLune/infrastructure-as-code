package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepo extends JpaRepository<Position, Long> {

    //* Looks for a <Position> in a <Board> by <x> and <y>.
    Position readPositionByTile_Area_Board_IdAndXAndY(long boardId, int x, int y);
}
