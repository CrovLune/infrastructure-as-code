package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GamesRepo extends JpaRepository<Game, Long> {
    Game readGameById(long id);
}
