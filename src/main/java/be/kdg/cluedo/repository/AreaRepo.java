package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreaRepo extends JpaRepository<Area, Long> {

    Area readAreaByBoardAndName(Board board, String areaName);

    Area readAreaByBoard_IdAndName(long boardId, String areaName);

    List<Area> readAllByBoard(Board board);

    List<Area> readAllByBoard_Id(long boardId);

}
