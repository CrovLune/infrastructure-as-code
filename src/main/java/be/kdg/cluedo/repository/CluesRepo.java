package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.clues.Clue;
import be.kdg.cluedo.domain.players.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CluesRepo extends JpaRepository<Clue, Long> {
    Clue findClueByOwnerAndCardAndSuspect(Player owner, Card card, Player suspect);

    Set<Clue> findCluesByOwner(Player player);
}
