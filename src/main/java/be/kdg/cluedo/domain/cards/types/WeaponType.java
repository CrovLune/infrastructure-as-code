package be.kdg.cluedo.domain.cards.types;

import lombok.Getter;

@Getter
public enum WeaponType {
    CANDLESTICK("Candlestick"),
    DAGGER("Dagger"),
    LEAD_PIPE("Lead Pipe"),
    REVOLVER("Revolver"),
    ROPE("Rope"),
    WRENCH("Wrench");

    public final String name;

    WeaponType(String name) {
        this.name = name;
    }
}
