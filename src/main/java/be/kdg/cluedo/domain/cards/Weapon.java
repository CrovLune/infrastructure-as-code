package be.kdg.cluedo.domain.cards;

import be.kdg.cluedo.domain.cards.types.WeaponType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
public class Weapon extends Card {
    private WeaponType weaponType;

    public Weapon(WeaponType weaponType) {
        super.setName(weaponType.getName());
        super.setType(this.getClass().getSimpleName());
        setWeaponType(weaponType);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
