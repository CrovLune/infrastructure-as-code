package be.kdg.cluedo.domain.cards;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.players.Player;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "cards")
public abstract class Card implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;
    private String name;
    private String type;

    //    @ManyToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "player_id", nullable = false)
//    @Transient
//    private Player player;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Board> boards;
//    private Boolean isInEnvelope = false;


    public Card() {
        this.boards = new LinkedHashSet<>();
        this.setType(this.getClass().getSimpleName());
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", this.getClass().getSimpleName(), this.getName());
    }
}
