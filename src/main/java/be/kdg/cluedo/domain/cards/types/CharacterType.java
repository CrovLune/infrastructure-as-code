package be.kdg.cluedo.domain.cards.types;

import be.kdg.cluedo.domain.Position;
import lombok.Getter;

@Getter
public enum CharacterType {
    MISS_SCARLETT("Miss Scarlett", new Position(0, 12)),
//    M_BRUNETTE("M. Brunette", new Position(14, 7)),
    COLONEL_MUSTARD("Colonel Mustard", new Position(12, 0)),
    MRS_WHITE("Mrs. White", new Position(7, 0)),
    MR_GREEN("Mr. Green", new Position(14, 12)),
    MRS_PEACOCK("Mrs. Peacock", new Position(12, 14)),
    PROFESSOR_PLUM("Professor Plum", new Position(0, 7)),
    MISS_PEACH("Miss Peach", new Position(7, 14));
//    MRS_MEAWDOWBROOK("Mrs. Brook", new Position(0, 2));


    public final String name;
    public final Position position;

    CharacterType(String name, Position position) {
        this.name = name;
        this.position = position;
    }
}
