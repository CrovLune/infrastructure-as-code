package be.kdg.cluedo.domain.cards;

import be.kdg.cluedo.domain.cards.types.CharacterType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
public class Character extends Card {
    private CharacterType characterType;

    public Character(CharacterType characterType) {
        super.setName(characterType.getName());
        super.setType(this.getClass().getSimpleName());
        setCharacterType(characterType);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
