package be.kdg.cluedo.domain.cards.types;

import lombok.Getter;

@Getter
public enum RoomType {
    LOUNGE("Lounge"),
    HALL("Hall"),
    STUDY("Study"),
    LIBRARY("Library"),
    KITCHEN("Kitchen"),
    BALLROOM("Ballroom"),
    CONSERVATORY("Conservatory"),
    BILLIARD_ROOM("Billiard Room"),
    DINING_ROOM("Dining Room");

    private final String name;

    RoomType(String name) {
        this.name = name;
    }
}
