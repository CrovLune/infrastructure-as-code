package be.kdg.cluedo.domain.cards;

import be.kdg.cluedo.domain.cards.types.RoomType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
public class Room extends Card {
    private RoomType roomType;

    public Room(RoomType roomType) {
        super.setName(roomType.getName());
        super.setType(this.getClass().getSimpleName());
        setRoomType(roomType);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
