package be.kdg.cluedo.domain.players;

import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.tiles.Tile;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "players")
public class Player implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;
    private String name;
    private CharacterType chosenCharacter;

    //?---------------------------------- Links to other tables ----------------------------------

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Game game;

    @OneToOne
    private Tile currentTile;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Card> cards;

    public Player() {
        this.cards = new LinkedHashSet<>();
    }

    public Player(String name, CharacterType chosenCharacter, Game game) {
        this.setName(name);
        this.setChosenCharacter(chosenCharacter);
        this.setGame(game);
        this.setCards(new LinkedHashSet<>());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name) && chosenCharacter == player.chosenCharacter && Objects.equals(game, player.game);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, chosenCharacter, game);
    }

    @Override
    public String toString() {
        var t = "";
        var p = "";
        var c = "";

        if (this.getCurrentTile() == null) {
            t = "NONE";
            p = "NONE";
        } else {
            t = String.valueOf(this.getCurrentTile().getId());
            p = this.getCurrentTile().getPosition().toString();
        }
        if (this.getCards().size() == 0) {
            c = "NONE";
        } else {
            c = this.getCards().toString();
        }
        return String.format("[%d] Name [%s] | Character [%s] | Game [%d] | Current Tile [%s] with Pos [%s] | Cards [%s]",
                this.getId(),
                this.getName(),
                this.getChosenCharacter(),
                this.getGame().getId(),
                t,
                p,
                c
        );
    }
}
