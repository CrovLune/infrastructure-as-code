package be.kdg.cluedo.domain.players;

import be.kdg.cluedo.domain.cards.Card;
import lombok.Data;

import java.util.List;

@Data
public class Result {
    private final List<Long> right;
    private final List<Long> wrong;

    public Result(List<Long> right, List<Long> wrong) {
        this.right = right;
        this.wrong = wrong;
    }

    @Override
    public String toString() {
        return String.format("[%s/%s] right",right.size(), (right.size() + wrong.size()));
    }
}
