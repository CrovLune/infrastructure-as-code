package be.kdg.cluedo.domain.players;

import be.kdg.cluedo.domain.cards.Room;
import be.kdg.cluedo.domain.cards.Character;
import be.kdg.cluedo.domain.cards.Weapon;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Suggestion {
    private Weapon weaponCard;
    private Room roomCard;
    private Character characterCard;

    public Suggestion(Weapon weaponCard, Room roomCard, Character characterCard) {
        this.weaponCard = weaponCard;
        this.roomCard = roomCard;
        this.characterCard = characterCard;
    }
}
