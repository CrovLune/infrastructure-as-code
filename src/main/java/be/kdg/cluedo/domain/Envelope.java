package be.kdg.cluedo.domain;

import be.kdg.cluedo.domain.cards.Character;
import be.kdg.cluedo.domain.cards.Room;
import be.kdg.cluedo.domain.cards.Weapon;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@Table(name = "envelopes")
@NoArgsConstructor
public class Envelope {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    private Weapon weaponCard;
    @OneToOne
    private Room roomCard;
    @OneToOne
    private Character characterCard;
    @OneToOne
    private Game game;

    public Envelope(Game game) {
        this.setGame(game);
    }

}
