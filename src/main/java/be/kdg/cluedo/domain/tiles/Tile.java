package be.kdg.cluedo.domain.tiles;

import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Position;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tiles")
public class Tile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int textureId;
    private TileType type;


    //?---------------------------------- Links to other tables ----------------------------------
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "area_id")
    private Area area;

    @OneToOne(mappedBy = "tile", cascade = CascadeType.ALL)
    private Position position;

    public Tile(Area area, int textureId, TileType type) {
        setArea(area);
        setTextureId(textureId);
        setType(type);
    }

    public Tile(int textureId, TileType type, Position position) {
        this.textureId = textureId;
        this.type = type;
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return id == tile.id && position.equals(tile.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position);
    }

    @Override
    public String toString() {
        //return String.valueOf(getType().name.charAt(0));

        //TODO: remove (debug only)

        //Show tiles with RoomType
        //return String.format("[ %s ]", isPlayerStandingOn() ? "P" : (isReachAble ? "." : String.valueOf(getRoomType().name.charAt(0))));

        //Show tiles with TileType
//        return String.format("[%s]", isPlayerStandingOn() ? " P " : String.format("%s%s%s", (isReachAble ? "-" : " "), String.valueOf(getType().name.charAt(0)), (isReachAble ? "-" : " ")));

        //Show tiles with Position
        //TODO: remove (debug only)
        return String.format("[%s]",this.position);
    }
}
