package be.kdg.cluedo.domain.tiles;

import java.util.Arrays;

public enum TileType {
    UNWALKABLE(0, "Unwalkable"),
    WALKABLE(1, "Walkable"),
    PORTAL(2, "Portal"),
    DOOR(3, "Doorway");

    public final int id;
    public final String name;

    TileType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static TileType tileTypeById(int id) {
        return Arrays.stream(TileType.values())
                .filter(v -> v.id == id).findFirst()
                .orElseThrow();
    }
}
