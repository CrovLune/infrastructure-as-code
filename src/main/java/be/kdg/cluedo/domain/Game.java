package be.kdg.cluedo.domain;

import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.players.Player;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.*;

@Data
@Component
@Entity
@Table(name = "games")
@AllArgsConstructor
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //?---------------------------------- Links to other tables ----------------------------------
    @OneToOne
    private Board board;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Card> cards;

    @OneToOne(cascade = CascadeType.ALL)
    private Player currentPlayer;

    public Game() {
        this.setCards(new LinkedHashSet<>());
    }

    public Game(Board board) {
        this.setCards(new LinkedHashSet<>());
        this.setBoard(board);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return id == game.id && Objects.equals(board, game.board);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, board);
    }
}
