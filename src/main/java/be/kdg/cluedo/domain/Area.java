package be.kdg.cluedo.domain;

import be.kdg.cluedo.domain.tiles.Tile;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "areas")
@Data
@NoArgsConstructor
public class Area implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    //?---------------------------------- Links to other tables ----------------------------------

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id")
    private Board board;

    @OneToMany(mappedBy = "area", cascade = CascadeType.ALL)
    private List<Tile> tiles;


    public Area(Board board, String name) {
        this.board = board;
        this.name = name;
        this.tiles = new ArrayList<>();
    }

    @Override
    public String toString() {
        return String.format("%s", this.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Area area = (Area) o;
        return id == area.id && Objects.equals(board, area.board);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, board);
    }
}
