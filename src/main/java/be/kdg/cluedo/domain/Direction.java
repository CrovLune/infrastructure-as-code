package be.kdg.cluedo.domain;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}
