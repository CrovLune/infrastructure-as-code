package be.kdg.cluedo.domain.websocket;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LobbyPlayer {
    private String character;
}
