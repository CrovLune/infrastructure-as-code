package be.kdg.cluedo.domain.clues;

import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.players.Player;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "clues")
@NoArgsConstructor
@AllArgsConstructor
public class Clue {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;

    private State state;

    //?---------------------------------- Links to other tables ----------------------------------
    @OneToOne(cascade = CascadeType.ALL)
    private Card card;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "suspect_id")
    private Player suspect;

    @ManyToOne(cascade = CascadeType.ALL)
    private Player owner;


    public Clue(Player owner, State state, Card card, Player suspect) {
        this.state = state;
        this.card = card;
        this.suspect = suspect;
        this.owner = owner;
    }
}
