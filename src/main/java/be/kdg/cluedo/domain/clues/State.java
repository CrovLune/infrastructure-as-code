package be.kdg.cluedo.domain.clues;

public enum State {
    PRESENT,NOT_PRESENT,QUESTION_MARK
}
