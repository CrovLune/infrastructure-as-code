package be.kdg.cluedo.domain;

import be.kdg.cluedo.factories.StartingPosition;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Data
@Entity
@Table(name = "boards")
public class Board implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    //?---------------------------------- Links to other tables ----------------------------------

    @OneToMany(mappedBy = "board", cascade = CascadeType.ALL)
    private List<Area> areas;

    @OneToMany(mappedBy = "board", cascade = CascadeType.ALL)
    private List<StartingPosition> startingPositions;

    public Board(String name) {
        this.name = name;
        this.areas = new ArrayList<>();
        this.startingPositions = new ArrayList<>();
        //        this.cards = new LinkedHashSet<>();
    }

    public Board() {
        this.areas = new ArrayList<>();
        this.startingPositions = new ArrayList<>();
    }

    @Override
    public String toString() {
        return String.format("[%s]",this.getName());
    }
}
