package be.kdg.cluedo.services;

import be.kdg.cluedo.properties.DiceProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Random;

@Service
public class DiceService {
    private final DiceProperties diceProperties;

    public DiceService(DiceProperties diceProperties) {
        this.diceProperties = diceProperties;
    }

    public ArrayList<Integer> throwDie() {
        //A list to figure out how much you threw for every die, based on boundaries specified in the properties file
        //This information could be useful to clients, for example showing n die and a simulated throw of your die/dice.
        ArrayList<Integer> throwList = new ArrayList<>(); //ArrayList is faster in accessing and storing data than a LinkedList
        Random worp = new Random();

        //For every die
        for (int i = 0; i < diceProperties.getTotal(); i++) {
            //Your highest throw
            throwList.add(worp.nextInt(diceProperties.getMaxSides()) + 1); //0 is inclusive, reduce max eyes by 1 and add 1 afterwards (If maxEyes is 6, then a throw between 0-6 is now 0-5 + 1)
        }

        return throwList;
    }
}
