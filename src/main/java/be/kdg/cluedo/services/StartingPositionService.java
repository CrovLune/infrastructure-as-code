package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.factories.StartingPosition;
import be.kdg.cluedo.repository.StartingPositionRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StartingPositionService {
    private final StartingPositionRepo repo;
    private final PositionService positionService;
    private final BoardService boardService;


    public StartingPosition save(StartingPosition position) {
        this.repo.save(position);
        return position;
    }

    public void setStartingPositions(Board board) {
        for (var character : CharacterType.values()) {
            var x = character.getPosition().getX();
            var y = character.getPosition().getY();
            var position = positionService.getPositionAt(board, x, y);
            var pos = new StartingPosition(board, character.getName(), position);

            repo.save(pos);
            board.getStartingPositions().add(pos);
        }
        boardService.save(board);
    }

    public StartingPosition getStartPosition(Board board, CharacterType type) {
        return this.repo.readStartingPositionByBoardAndCharacterName(board, type.getName());
    }
}
