package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.messaging.*;
import be.kdg.cluedo.messaging.dto.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Service
public class MessengerService {
    private final GameStartMessenger gameStartMessenger;
    private final GameEndMessenger gameEndMessenger;
    private final GuessMessenger guessMessenger;
    private final MoveMessenger moveMessenger;
    private final SessionStartMessenger sessionStartMessenger;
    private final SessionEndMessenger sessionEndMessenger;
    private final CardService cardService;
    private final GameService gameService;

    public void sendStartOfGame(Game game) {
        GameStartDTO gameStartDTO = new GameStartDTO();
        gameStartDTO.setGameID((int) game.getId());
        gameStartDTO.setGameStartDateTime(LocalDateTime.now());
        gameStartMessenger.sendStartOfGame(gameStartDTO);
    }

    public void sendStartUsersSession(List<Player> players, Game game) {
        for (var player : players) {
            this.sendStartUserSession(player, game);
        }
    }

    public void sendStartUserSession(Player player, Game game) {
        UserSessionStartDTO userSessionStartDTO = new UserSessionStartDTO();
        userSessionStartDTO.setUserID((int) player.getId());
        userSessionStartDTO.setGameID((int) game.getId());
        userSessionStartDTO.setSessionStartDateTime(LocalDateTime.now());
        sessionStartMessenger.sendStartOfUserSession(userSessionStartDTO);
    }

    public void sendMove(long playerID, long gameID) {
        MoveDTO moveDTO = new MoveDTO();
        moveDTO.setGameID((int) gameID);
        moveDTO.setUserID((int) playerID);
        moveMessenger.sendMove(moveDTO);
    }

    public void sendGuess(long gameID, long playerID, long roomId, long weaponId, long characterId) {
        GuessDTO guessDTO = new GuessDTO();
        guessDTO.setGameID((int) gameID);
        guessDTO.setUserID((int) playerID);
        guessDTO.setRoom(this.cardService.getCard(this.gameService.getGameById(gameID), roomId).getName());
        guessDTO.setWeapon(this.cardService.getCard(this.gameService.getGameById(gameID), weaponId).getName());
        guessDTO.setCharacter(this.cardService.getCard(this.gameService.getGameById(gameID), characterId).getName());
        guessMessenger.sendGuess(guessDTO);
    }

    public void sendEndAllUsersSession(List<Player> players, int gameID) {
        for (Player player :
                players) {
            sendEndUserSession((int) player.getId(), gameID);
        }
    }

    public void sendEndUserSession(int playerID, int gameID) {
        UserSessionEndDTO userSessionEndDTO = new UserSessionEndDTO();
        userSessionEndDTO.setGameID(gameID);
        userSessionEndDTO.setUserID(playerID);
        userSessionEndDTO.setWon(false);
        userSessionEndDTO.setSessionEndDateTime(LocalDateTime.now());
        sessionEndMessenger.sendEndOfUserSession(userSessionEndDTO);
    }
    public void sendEndWonUserSession(int playerID, int gameID) {
        UserSessionEndDTO userSessionEndDTO = new UserSessionEndDTO();
        userSessionEndDTO.setUserID(playerID);
        userSessionEndDTO.setGameID(gameID);
        userSessionEndDTO.setWon(true);
        userSessionEndDTO.setSessionEndDateTime(LocalDateTime.now());
        sessionEndMessenger.sendEndOfUserSession(userSessionEndDTO);
    }

    public void endGame(int gameID) {
        GameEndDTO gameEndDTO = new GameEndDTO();
        gameEndDTO.setGameID(gameID);
        gameEndDTO.setGameEndDateTime(LocalDateTime.now());
        gameEndMessenger.sendEndOfGame(gameEndDTO);
    }
}
