package be.kdg.cluedo.services.restServices;

import be.kdg.cluedo.services.BoardService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class BoardRestService {
    private final BoardService service;

    public List<String> getAllBoards() {
        var boards = this.service.getAllBoards();
        List<String> boardNames = new ArrayList<>();

        for (var board : boards) {
            boardNames.add(board.getName());
        }
        return boardNames;
    }
}
