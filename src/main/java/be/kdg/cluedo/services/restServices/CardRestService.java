package be.kdg.cluedo.services.restServices;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.cards.Character;
import be.kdg.cluedo.repository.CardsRepo;
import be.kdg.cluedo.services.CardService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CardRestService {
    private final CardService service;
    private final CardsRepo repo;

    public List<String> getAllCharacters(Board board) {
        var cards = this.repo.readCardsByBoardsContainingAndType(board, Character.class.getSimpleName());
        return cards.stream().map(Card::getName).collect(Collectors.toList());
    }
}
