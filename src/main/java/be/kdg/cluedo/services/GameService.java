package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiSuggestion;
import be.kdg.cluedo.controllers.dtos.suggestion.SuggestionDTO;
import be.kdg.cluedo.domain.Envelope;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.domain.players.Result;
import be.kdg.cluedo.repository.GamesRepo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Getter
@Setter
@AllArgsConstructor
public class GameService {
    private final GamesRepo repo;

    public void save(Game game) {
        repo.save(game);
    }

    private void fillLists(Boolean isFound, Long cardId, List<Long> right, List<Long> wrong) {
        if (isFound) {
            right.add(cardId);
        } else {
            wrong.add(cardId);
        }
    }

    public Result checkAccusation(RiSuggestion suggestion, Envelope envelope) {
        var weaponCheck = suggestion.getWeaponCardId() == envelope.getWeaponCard().getId();
        var characterCheck = suggestion.getCharacterCardId() == envelope.getCharacterCard().getId();
        var roomCheck = suggestion.getRoomCardId() == envelope.getRoomCard().getId();

        List<Long> right = new ArrayList<>();
        List<Long> wrong = new ArrayList<>();

        this.fillLists(weaponCheck, suggestion.getWeaponCardId(), right, wrong);
        this.fillLists(characterCheck, suggestion.getCharacterCardId(), right, wrong);
        this.fillLists(roomCheck, suggestion.getRoomCardId(), right, wrong);

        return new Result(right, wrong);
    }

    public List<Long> checkSuggestion(SuggestionDTO suggestionDTO, Player nextPlayerInRow) {
        var cards = nextPlayerInRow.getCards();
        var filtered = cards.stream()
                .filter(
                        x -> x.getId() == suggestionDTO.getSuggestion().getCharacterCardId()
                                || x.getId() == suggestionDTO.getSuggestion().getRoomCardId()
                                || x.getId() == suggestionDTO.getSuggestion().getWeaponCardId()
                )
                .collect(Collectors.toList());

        return filtered.stream().map(Card::getId).collect(Collectors.toList());
    }

    public Game getGameById(long id) {
        return repo.readGameById(id);
    }

    public List<Game> getAllGames() {
        return repo.findAll();
    }

}
