package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Position;
import be.kdg.cluedo.repository.PositionRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class PositionService {
    private final PositionRepo repo;

    public Position save(Position position) {
        repo.save(position);
        return position;
    }

    public Position getPositionAt(Board board, int x, int y) {
        return this.getPositionAt(board.getId(), x, y);
    }

    public Position getPositionAt(long boardId, int x, int y) {
        return repo.readPositionByTile_Area_Board_IdAndXAndY(boardId, x, y);
    }
}
