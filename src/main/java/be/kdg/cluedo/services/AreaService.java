package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.cards.types.RoomType;
import be.kdg.cluedo.repository.AreaRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AreaService {
    private final AreaRepo repo;

    public void save(Area area) {
        repo.save(area);
    }

    public Area getAreaBy(Board board, RoomType roomName) {
        return repo.readAreaByBoardAndName(board, roomName.toString());
    }

    public Area getAreaBy(long boardId, RoomType roomName) {
        return repo.readAreaByBoard_IdAndName(boardId, roomName.toString());
    }

    public List<Area> getAllAreasBy(Board board) {
        return repo.readAllByBoard(board);
    }

    public List<Area> getAllAreasBy(long boardId) {
        return repo.readAllByBoard_Id(boardId);
    }
}
