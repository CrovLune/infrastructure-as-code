package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.Card;
import be.kdg.cluedo.domain.cards.Character;
import be.kdg.cluedo.domain.cards.Room;
import be.kdg.cluedo.domain.cards.Weapon;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.cards.types.RoomType;
import be.kdg.cluedo.domain.cards.types.WeaponType;
import be.kdg.cluedo.repository.CardsRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class CardService {
    private final CardsRepo repo;
    private final EnvelopeService envelopeService;
    private final PlayerService playerService;
    private final GameService gameService;

    public Card save(Card card) {
        repo.save(card);
        return card;
    }

    public List<Card> saveAll(List<Card> cards) {
        repo.saveAll(cards);
        return cards;
    }

    public void putCardsInGame(Game game) {
        var board = game.getBoard();
        var cards = repo.readCardsByBoardsContaining(board);
        this.putRandomCardsInEnvelope(cards, game);
    }

    public List<Weapon> getWeaponCards(Game game) {
        var weaponCards = game.getCards()
                .stream()
                .filter(x -> x.getClass() == Weapon.class)
                .map(x -> (Weapon) x)
                .collect(Collectors.toList());

        Collections.shuffle(weaponCards);

        return weaponCards;
    }

    public List<Character> getCharacterCards(Game game) {
        var personCards = game.getCards()
                .stream()
                .filter(x -> x.getClass() == Character.class)
                .map(x -> (Character) x)
                .collect(Collectors.toList());

        Collections.shuffle(personCards);

        return personCards;
    }

    public List<Room> getRoomCards(Game game) {
        var locationCards = game.getCards()
                .stream()
                .filter(x -> x.getClass() == Room.class)
                .map(x -> (Room) x)
                .collect(Collectors.toList());

        Collections.shuffle(locationCards);

        return locationCards;
    }

    public List<Weapon> getWeaponCards(List<Card> cards) {
        var weaponCards = cards
                .stream()
                .filter(x -> x.getClass() == Weapon.class)
                .map(x -> (Weapon) x)
                .collect(Collectors.toList());

        Collections.shuffle(weaponCards);

        return weaponCards;
    }

    public List<Character> getCharacterCards(List<Card> cards) {
        var personCards = cards
                .stream()
                .filter(x -> x.getClass() == Character.class)
                .map(x -> (Character) x)
                .collect(Collectors.toList());

        Collections.shuffle(personCards);

        return personCards;
    }

    public List<Room> getRoomCards(List<Card> cards) {
        var locationCards = cards
                .stream()
                .filter(x -> x.getClass() == Room.class)
                .map(x -> (Room) x)
                .collect(Collectors.toList());

        Collections.shuffle(locationCards);

        return locationCards;
    }


    //* Takes list of cards, puts 3 random in envelope.
    private void putRandomCardsInEnvelope(List<Card> cards, Game game) {
        var r = new Random();
        var envelope = this.envelopeService.getEnvelopeFor(game);

        var weaponCards = this.getWeaponCards(new ArrayList<>(cards));
        var characterCards = this.getCharacterCards(new ArrayList<>(cards));
        var roomCards = this.getRoomCards(new ArrayList<>(cards));


        var weaponCard = weaponCards.remove(r.nextInt(weaponCards.size()));
        var characterCard = characterCards.remove(r.nextInt(characterCards.size()));
        var roomCard = roomCards.remove(r.nextInt(roomCards.size()));

        game.getCards().clear();
        game.getCards().addAll(cards);


        //* Add 1 card of all 3 types to the envelope
        envelope.setWeaponCard(weaponCard);
        envelope.setCharacterCard(characterCard);
        envelope.setRoomCard(roomCard);

        this.envelopeService.save(envelope);

        this.gameService.save(game);
    }

    public void spreadCardsOverPlayers(Game game) {
        var cards = new ArrayList<>(game.getCards());
        var envelope = this.envelopeService.getEnvelopeFor(game);
        cards.removeIf(x ->
                envelope.getRoomCard() == x
                        || envelope.getCharacterCard() == x
                        || envelope.getWeaponCard() == x);
        var players = this.playerService.getPlayersFor(game);
        Collections.shuffle(cards);
        var size = cards.size();
        do {
            for (var player : players) {
                if (cards.size() > 0) {
                    player.getCards().add(cards.remove(0));
                    this.playerService.save(player);
                }
            }
            size--;
        } while (size > 0);
    }

    //TODO: DELETE
    public void addBoardToCards(Board board) {
        for (int i = 0; i < WeaponType.values().length; i++) {
            var card = new Weapon(WeaponType.values()[i]);
            this.linkCardToBoard(card, board);
        }
        for (int i = 0; i < RoomType.values().length; i++) {
            var card = new Room(RoomType.values()[i]);
            this.linkCardToBoard(card, board);
        }
        for (int i = 0; i < CharacterType.values().length; i++) {
            var card = new Character(CharacterType.values()[i]);
            this.linkCardToBoard(card, board);
        }
    }

    public void linkCardToBoard(Card card, Board board) {
        var lookForCard = this.repo.findAll().stream()
                .filter(x -> x.getName().equals(card.getName())
                        && x.getClass().getSimpleName().equals(card.getClass().getSimpleName()))
                .findFirst()
                .orElse(null);

        if (lookForCard == null) {
            card.getBoards().add(board);

            this.repo.save(card);
        } else {
            lookForCard.getBoards().add(board);
            this.repo.save(lookForCard);
        }
    }

    public Card getCard(Game game, Long cardId) {
        return game.getCards()
                .stream()
                .filter(x -> x.getId() == cardId)
                .findFirst()
                .orElseThrow();
    }

    public List<Card> getCardsFromIds(List<Long> cardsIdsList) {
        return null;
    }
}
