package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiBoard;
import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.factories.Factory;
import be.kdg.cluedo.repository.BoardRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class BoardService {
    //TODO: Create validators per map
    //TODO: Add change player position when he/she is being suggested;

    private final Factory factory;
    private final BoardRepo repo;

    public void save(Board board) {
        repo.save(board);
    }

    public List<Board> getAllBoards() {
        return repo.findAll();
    }

    public Board getBoardBy(String name) {
        return repo.readBoardByName(name);
    }

    public Board getBoardBy(long boardId) {
        return repo.readBoardById(boardId);
    }

    public Board getBoardBy(Area area) {
        return repo.readBoardByAreasIsContaining(area);
    }

    public RiBoard convertToRiBoard(Board board) {
        var riboard = new RiBoard();
        riboard.setId(board.getId());
        riboard.setName(board.getName());
        for (var area : board.getAreas()) {
            riboard.getAreas().add(area.getId());
        }
        for (var pos : board.getStartingPositions()) {
            riboard.getStartingPositions().add(pos.getId());
        }
        return riboard;
    }
}
