package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Envelope;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.repository.EnvelopeRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EnvelopeService {
    private final EnvelopeRepo repo;

    public void save(Envelope envelope) {
        repo.save(envelope);
    }

    public Envelope getEnvelopeFor(Game game) {
        return repo.readEnvelopeByGame(game);
    }

    public Envelope getEnvelopeFor(Long gameId) {
        return repo.readEnvelopeByGame_Id(gameId);
    }
}
