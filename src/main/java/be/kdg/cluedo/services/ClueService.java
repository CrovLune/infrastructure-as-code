package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.clues.Clue;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.repository.CluesRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class ClueService {
    private final CluesRepo repo;

    public void save(Clue clue) {
        repo.save(clue);
    }

    public void changeClue(Clue clue) {
        var clueFromDb = this.repo.findClueByOwnerAndCardAndSuspect(clue.getOwner(), clue.getCard(), clue.getSuspect());

        if (clueFromDb != null) {
            clueFromDb.setState(clue.getState());
            this.repo.save(clueFromDb);
        } else {
            this.repo.save(clue);
        }
    }

    public Set<Clue> getCluesOfPlayer(Player player) {
        return this.repo.findCluesByOwner(player);
    }
}
