package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.player.PlayerDTO;
import be.kdg.cluedo.controllers.dtos.player.PlayerPositionDTO;
import be.kdg.cluedo.controllers.dtos.relevantinfo.RiPlayer;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.domain.tiles.Tile;
import be.kdg.cluedo.repository.PlayerRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static be.kdg.cluedo.domain.tiles.TileType.UNWALKABLE;
import static be.kdg.cluedo.domain.tiles.TileType.WALKABLE;

@Service
@AllArgsConstructor
@Slf4j
public class PlayerService {
    private final PlayerRepo repo;
    private final StartingPositionService startingPositionService;
    private final TileService tileService;

    public void save(Player player) {
        repo.save(player);
    }

    public List<Player> getAllPlayers() {
        return this.repo.findAll();
    }

    public Player getPlayerBy(Long playerId) {
        return this.repo.readPlayerById(playerId);
    }

    public List<Player> getPlayersFor(Game game) {
        return this.repo.readPlayersByGame(game);
    }

    public List<Player> getPlayersFor(Long gameId) {
        return this.repo.readPlayersByGame_Id(gameId);
    }

    public void UpdateCurrentTile(Player player, Tile newTile) {
        //* Make new tile UNWALKABLE since player is on it.
//        newTile.setType(UNWALKABLE);
//        this.tileService.save(newTile);

        //* make current tile WALKABLE again since player moved from the tile and it's free again.
        var currentTile = player.getCurrentTile();
//        currentTile.setType(WALKABLE);
//        this.tileService.save(currentTile);

        player.setCurrentTile(newTile);
        this.repo.save(player);
    }

    public List<Player> createPlayersFor(List<PlayerDTO> players, Game game) {
        List<Player> pls = new ArrayList<>();
        for (var player : players) {
            var p = new Player(player.getName(), CharacterType.valueOf(player.getChosenCharacter()), game);
            this.repo.save(p);
            pls.add(p);
        }
        return pls;
    }

    public void setStartPosition(Game game) {
        log.info("Setting Position Started");

        var players = this.getPlayersFor(game);
        System.out.println(players);
        for (var player : players) {
            var board = game.getBoard();
            var startPos = this.startingPositionService.getStartPosition(board, player.getChosenCharacter());
            var x = startPos.getPosition().getX();
            var y = startPos.getPosition().getY();
            var tile = this.tileService.getTileAt(board, x, y);

//            tile.setType(UNWALKABLE);
//            this.tileService.save(tile);

            player.setCurrentTile(tile);

            this.repo.save(player);
        }
    }

    public List<PlayerPositionDTO> getPlayerPositions(Game game) {
        var players = this.getPlayersFor(game);
        List<PlayerPositionDTO> positions = new ArrayList<>();
        for (var player : players) {
            var ps = new PlayerPositionDTO(player.getName(), player.getCurrentTile());
            positions.add(ps);
        }
        return positions;
    }

    public void setCurrentPlayer(Game game) {
        var players = this.getPlayersFor(game);
        game.setCurrentPlayer(players.get(0));
    }

    public Set<Tile> getStepsForPlayer(Player player, Board board, int diceEyes, List<Player> players) {
        var currentTile = player.getCurrentTile();
        return this.tileService.getTilesAround(board, currentTile, diceEyes, players);
    }

    public List<RiPlayer> convertToRiPlayers(List<Player> players) {
        List<RiPlayer> riPlayerList = new ArrayList<>();
        for (var player : players) {
            Set<Long> cards = new HashSet<>();
            for (var card : player.getCards()) {
                cards.add(card.getId());
            }
            var riPlayer = new RiPlayer(
                    player.getId(),
                    player.getName(),
                    player.getChosenCharacter(),
                    player.getGame().getId(),
                    player.getCurrentTile().getPosition().getX(),
                    player.getCurrentTile().getPosition().getY(),
                    cards
            );
            riPlayerList.add(riPlayer);
        }
        return riPlayerList;
    }
    //todo
    //Spelers kunnen ten alle tijden zien welke speler aan de beurt is
    //Wie is er aan de beurt, waar staat de speler, wat mag/kan de speler doen...

}
