package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiValidTile;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Direction;
import be.kdg.cluedo.domain.cards.types.RoomType;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.domain.tiles.Tile;
import be.kdg.cluedo.repository.TileRepo;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static be.kdg.cluedo.domain.cards.types.RoomType.BALLROOM;
import static be.kdg.cluedo.domain.cards.types.RoomType.BILLIARD_ROOM;
import static be.kdg.cluedo.domain.tiles.TileType.*;

@Service
@AllArgsConstructor
@Slf4j
public class TileService {
    private final TileRepo repo;

    public void save(Tile tile) {
        this.repo.save(tile);
    }

    public Tile getTileAt(Board board, Long tileId) {
        return this.repo.readTilesByArea_BoardAndId(board, tileId);
    }

    public List<Tile> getAllAvailableTiles(Board board) {
        return getAllTilesOf(board)
                .stream()
                .filter(x -> x.getType() != UNWALKABLE)
                .collect(Collectors.toList());
    }

    public Tile getTileAt(Board board, int x, int y) {
        return repo.readTileByArea_BoardAndPosition_xAndPosition_y(board, x, y);
    }

    public List<Tile> getTilesAt(Board board, RoomType roomName) {
        return repo.readTilesByArea_BoardAndArea_Name(board, roomName.toString());
    }

    public Tile getPortalTile(Board board, RoomType roomName) {
        var tiles = this.getTilesAt(board, roomName);
        return tiles.stream()
                .filter(x -> x.getType() == PORTAL)
                .findFirst()
                .orElseThrow();
    }

    public List<Tile> getPortalTiles(Board board) {
        var tiles = this.getAllAvailableTiles(board);
        return tiles.stream()
                .filter(x -> x.getType() == PORTAL)
                .collect(Collectors.toList());
    }

    public List<Tile> getAllTilesOf(Board board) {
        return repo.readTilesByArea_Board(board);
    }

    private List<Tile> getTilesAround(Board board, Tile tile) {
        List<Tile> tilesAround = new ArrayList<>();
        for (var direction : Direction.values()) {
            tilesAround.add(getTileInDirection(board, tile, direction));
        }
        tilesAround = tilesAround.stream()
                .filter(x -> x != tile)
                .collect(Collectors.toList());
        return tilesAround;
    }

    @SneakyThrows
    public Set<Tile> getTilesAround(Board board, Tile fromTile, int range, List<Player> players) {
        Set<Tile> tilesAround = Collections.synchronizedSet(new LinkedHashSet<>());
        Set<Tile> alreadyDone = Collections.synchronizedSet(new LinkedHashSet<>());
        tilesAround.add(fromTile);
        do {
            for (var tile : new ArrayList<>(tilesAround)) {
                Collection<Callable<Tile>> collection = Collections.synchronizedList(new ArrayList<>());

                for (var direction : Direction.values()) {
                    Callable<Tile> callable = () -> this.getTileInDirection(board, tile, direction);
                    collection.add(callable);
                }

                ExecutorService executorService = Executors.newFixedThreadPool(4);
                var found = this.calculate(executorService, collection);
                executorService.shutdown();

                found.remove(tile);
                found.removeIf(x -> x.getType() == UNWALKABLE);
                found.removeIf(x -> !x.getArea().getName().equals(tile.getArea().getName()) && x.getType() != DOOR && x.getType() != PORTAL);
                found.removeIf(alreadyDone::contains);
                found.removeIf(tilesAround::contains);
                found.removeIf(x -> players.stream()
                        .map(Player::getCurrentTile)
                        .map(Tile::getPosition)
                        .collect(Collectors.toList())
                        .contains(x.getPosition()));
                alreadyDone.add(tile);
                tilesAround.remove(tile);
                tilesAround.addAll(found);
            }
            range--;
        } while (range + 1 > 0);
        alreadyDone.remove(fromTile); // removing at last the tile you started from.
        System.out.println("FINAL RESULT: " + alreadyDone.size());
        System.out.println(alreadyDone);
        return alreadyDone;
    }

    private List<Tile> calculate(Executor e, Collection<Callable<Tile>> tiles) throws InterruptedException {
        CompletionService<Tile> ecs = new ExecutorCompletionService<>(e);
        List<Tile> completed = Collections.synchronizedList(new ArrayList<>());
        for (var callable : tiles) {
            ecs.submit(callable);
        }

        for (var x : tiles) {
            try {
                var t = ecs.take().get();
                if (t != null) {
                    completed.add(t);
                }
            } catch (ExecutionException ignore) {
            }
        }
        return completed;
    }

    public Tile getTileInDirection(Board board, Tile fromTile, Direction direction) {
        var x = fromTile.getPosition().getX();
        var y = fromTile.getPosition().getY();
        var size = 14;

        Tile toTile = new Tile();
        var cantMoveUp = y <= 0;
        var cantMoveLeft = x <= 0;
        var cantMoveDown = y >= size;
        var cantMoveRight = x >= size;

        switch (direction) {
            case UP:
                if (!cantMoveUp) {
                    toTile = this.getTileAt(board, fromTile.getPosition().getX(), fromTile.getPosition().getY() - 1);
                } else {
                    return fromTile;
                }
                break;
            case RIGHT:
                if (!cantMoveRight) {
                    toTile = this.getTileAt(board, fromTile.getPosition().getX() + 1, fromTile.getPosition().getY());
                } else {
                    return fromTile;
                }
                break;
            case DOWN:
                if (!cantMoveDown) {
                    toTile = this.getTileAt(board, fromTile.getPosition().getX(), fromTile.getPosition().getY() + 1);
                } else {
                    return fromTile;
                }
                break;
            case LEFT:
                if (!cantMoveLeft) {
                    toTile = this.getTileAt(board, fromTile.getPosition().getX() - 1, fromTile.getPosition().getY());
                } else {
                    return fromTile;
                }
                break;
        }
        if (toTile.getType() == PORTAL) {
            toTile = this.getPortalTileDestination(board, toTile);
        }
        return toTile;
    }

    public Tile getPortalTileDestination(Board board, Tile portalTile) {
        switch (RoomType.valueOf(portalTile.getArea().toString())) {
            case LOUNGE:
            case BALLROOM:
                return this.getPortalTile(board, BILLIARD_ROOM);
            case BILLIARD_ROOM:
                return this.getPortalTile(board, BALLROOM);
            default:
                return portalTile;
        }
    }

    public Set<RiValidTile> convertTilesToRiTiles(Set<Tile> tiles) {
        Set<RiValidTile> riTiles = new HashSet<>();
        for (var tile : tiles) {
            riTiles.add(new RiValidTile(tile.getPosition().getX(), tile.getPosition().getY()));
        }
        return riTiles;
    }
}
