package be.kdg.cluedo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
public class CluedoApplication {
    public static void main(String[] args) {
        SpringApplication.run(CluedoApplication.class, args);
    }
}
