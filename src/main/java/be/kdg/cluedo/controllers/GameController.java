package be.kdg.cluedo.controllers;

import be.kdg.cluedo.controllers.dtos.DiceRollDTO;
import be.kdg.cluedo.controllers.dtos.GameDTO;
import be.kdg.cluedo.controllers.dtos.GameEndDTO;
import be.kdg.cluedo.controllers.dtos.player.UpdatePlayerPositionDTO;
import be.kdg.cluedo.controllers.dtos.relevantinfo.RiBoard;
import be.kdg.cluedo.controllers.dtos.relevantinfo.RiPlayer;
import be.kdg.cluedo.controllers.dtos.suggestion.AccusationDTO;
import be.kdg.cluedo.controllers.dtos.suggestion.SuggestionDTO;
import be.kdg.cluedo.domain.Envelope;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.players.Result;
import be.kdg.cluedo.services.*;
import be.kdg.cluedo.services.restServices.CardRestService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/game-service")
@AllArgsConstructor
@Slf4j
/**
 * Handles all the GameLogic to GameUI communication.
 *
 * @Author : Zelenskiy Aleksey
 */
//TODO: Split in multiple controllers
public class GameController {
    private final GameService gameService;
    private final BoardService boardService;
    private final CardService cardService;
    private final PlayerService playerService;
    private final EnvelopeService envelopeService;
    private final TileService tileService;
    private final DiceService diceService;
    private final MessengerService messengerService;

    /**
     * Get a list of all Characters in a given Board.
     *
     * @Author : Zelenskiy Aleksey
     * @Param: gameDTO consists of a list of Players and a chosen Board name.
     * @See GameDTO
     * @Return : ResponseEntity<Long> Will return the Game ID of the created Game.
     */
    //TODO: Fix Exception Handling
    @PostMapping("/startGame")
    public ResponseEntity<Long> startGame(@RequestBody GameDTO gameDTO) {
        var board = this.boardService.getBoardBy(gameDTO.getBoardName());
        var game = new Game(board);
        this.gameService.save(game);
        var pls = this.playerService.createPlayersFor(gameDTO.getPlayers(), game);
        var envelope = new Envelope(game);
        this.envelopeService.save(envelope);
        this.cardService.putCardsInGame(game);
        this.cardService.spreadCardsOverPlayers(game);
        this.playerService.setStartPosition(game);
        this.playerService.setCurrentPlayer(game);
        gameService.save(game);

        messengerService.sendStartOfGame(game);
        messengerService.sendStartUsersSession(pls, game);
        return ResponseEntity.ok(game.getId());
    }

    @PostMapping("/endGame")
    //* Board + Player Name + Chosen Character = GameDTO
    public ResponseEntity<Void> endGame(@RequestBody GameEndDTO gameEndDTO) {
        messengerService.endGame((int) gameEndDTO.getGameID());
        messengerService.sendEndAllUsersSession(playerService.getPlayersFor(gameEndDTO.getGameID()), (int) gameEndDTO.getGameID());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Get a list of Card IDs that are found in the Cards Deck of another player.
     * <p>
     * Front-End sends request with the player-Suggestion + the playerId of the player that comes next.
     * CASE1: Front-End receives a result in form of <List<Long>> which indicates that next player has one or more cards.
     * -> Front-End proposes to "next player" the option to choose what card he wants to display to "player"
     * -> Front-End shows card to "player"
     * CASE2: Front-End receives <NOT_FOUND> which indicates that next player has NO cards that Player suggested.
     * -> Front-End Sends new (checkSuggestion) request with the player-ID of the player that comes after.
     * This happens till last player is reached -> checked by Front-End itself.
     *
     * @Author : Zelenskiy Aleksey
     * @Param: suggestionDTO consists of RiSuggesion & ID of the player is next to the player that is doing the suggestion.
     * @See RiSuggestion
     * @Return : ResponseEntity<List<Long>> Will return a List of Card IDs that were found in the players Deck.
     */
    @PostMapping("/checkSuggestion")
    public ResponseEntity<List<Long>> checkSuggestion(@RequestBody SuggestionDTO suggestionDTO) {

        var player = this.playerService.getPlayerBy(suggestionDTO.getNextPlayerInRowID());
        var result = this.gameService.checkSuggestion(suggestionDTO, player);

        messengerService.sendGuess(suggestionDTO.getGameID(), suggestionDTO.getCurrentPlayerID(), suggestionDTO.getSuggestion().getRoomCardId(), suggestionDTO.getSuggestion().getWeaponCardId(), suggestionDTO.getSuggestion().getCharacterCardId());
        return ResponseEntity.ok(result);
    }

    /**
     * Checks the Accusations against the Contents of the Envelope.
     * <p>
     * Front-End sends Request with player-Suggestion a.k.a. Accusation + Game-ID.
     * Front-End receives a <Result> which contains 2 List<Card> (Right/Wrong).
     * Depending on the amounts in the lists Front-End can decide whether the game is at End or not.
     * Front-End can also easily show the player which cards he had wrong/right.
     *
     * @Author : Zelenskiy Aleksey
     * @Param : accusationDTO consists of RiAccusation & Game ID.
     * @See RiAccusation
     * @Return : ResponseEntity<List<Result>> Returns 2 Lists containing ID's (right/wrong) of the cards that were guessed right or wrong accordingly.
     */
    @PostMapping("/checkAccusation")
    public ResponseEntity<Result> checkAccusation(@RequestBody AccusationDTO accusationDTO) {
        messengerService.sendGuess(accusationDTO.getGameId(), accusationDTO.getPlayerId(), accusationDTO.getSuggestion().getRoomCardId(), accusationDTO.getSuggestion().getWeaponCardId(), accusationDTO.getSuggestion().getCharacterCardId());
        var envelope = this.envelopeService.getEnvelopeFor(accusationDTO.getGameId());
        var check = this.gameService.checkAccusation(accusationDTO.getSuggestion(), envelope);

        return ResponseEntity.ok(check);
    }

    /**
     * Rolls the Dice. Generates random amount of eyes. Calculates available Tiles.
     *
     * @Author : Zelenskiy Aleksey
     * @Param: gameId The Game ID.
     * @Param: playerId The ID of the Player that is throwing the dice.
     * @Return: DiceRollDTO  Returns the amount of eyes & availableTiles for Player to move.
     * @See DiceRollDTO
     */
    @GetMapping("/rollDice/{gameId}/{playerId}")
    public ResponseEntity<DiceRollDTO> rollTheDice(
            @PathVariable(value = "gameId") Long gameId,
            @PathVariable(value = "playerId") Long playerId
    ) {
        var game = this.gameService.getGameById(gameId);
        var board = game.getBoard();
        var diceEyes = this.diceService.throwDie().get(0);
        var player = this.playerService.getPlayerBy(playerId);
        var players = this.playerService.getPlayersFor(game);
        var tiles = this.playerService.getStepsForPlayer(player, board, diceEyes, players);
        var riTiles = this.tileService.convertTilesToRiTiles(tiles);
        var dicerollDTO = new DiceRollDTO(diceEyes, riTiles);

        return ResponseEntity.ok(dicerollDTO);
    }
}
