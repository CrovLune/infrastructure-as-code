package be.kdg.cluedo.controllers;

import be.kdg.cluedo.services.BoardService;
import be.kdg.cluedo.services.restServices.CardRestService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/game-service")
public class CardController {
    //TODO: What is this?
    private final CardRestService cardRestService;
    private final BoardService boardService;

    /**
     * Get a list of all Characters in a given Board.
     *
     * @Author : Zelenskiy Aleksey
     * @Param: boardId The Board ID for which the characters are required.
     * @Return : ResponseEntity<List<String>> Will return a List of Character names for {boardId}.
     * @Throws: NotFoundException on not providing the {boardId}.
     */
    @GetMapping("/getAllCharacters/{boardId}")
    public ResponseEntity<List<String>> getAllCharacters(@PathVariable Long boardId) {
        var board = this.boardService.getBoardBy(boardId);
        var characters = this.cardRestService.getAllCharacters(board);

        if (!characters.isEmpty()) {
            return ResponseEntity.ok(characters);
        }

        return ResponseEntity.notFound().build();
    }
}
