package be.kdg.cluedo.controllers;

import be.kdg.cluedo.controllers.dtos.player.UpdatePlayerPositionDTO;
import be.kdg.cluedo.controllers.dtos.relevantinfo.RiPlayer;
import be.kdg.cluedo.services.GameService;
import be.kdg.cluedo.services.MessengerService;
import be.kdg.cluedo.services.PlayerService;
import be.kdg.cluedo.services.TileService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/game-service")
public class PlayerController {
    private final PlayerService playerService;
    private final GameService gameService;
    private final TileService tileService;
    private final MessengerService messengerService;

    /**
     * Gets the Players of a given Game.
     *
     * @Author : Zelenskiy Aleksey
     * @Param : gameId.
     * @Return : ResponseEntity<List<RiPlayer>> This returns a short version of Players, that contains only ID's instead of Objects.
     * @See RiPlayer
     */
    @GetMapping("/getPlayers/{gameId}")
    public ResponseEntity<List<RiPlayer>> getPlayers(@PathVariable(value = "gameId") Long gameId) {
        var game = this.gameService.getGameById(gameId);
        var players = this.playerService.getPlayersFor(game);
        var riPlayerList = this.playerService.convertToRiPlayers(players);

        return ResponseEntity.ok(riPlayerList);
    }

    /**
     * Updates the Position of a given Player with a given Tile.
     *
     * @Author : Zelenskiy Aleksey
     * @Param: gameId The game ID.
     * @Param: changes Is a UpdatePlayerDTO, which consists of playerId and TileId.
     * @See UpdatePlayerDTO
     * @Return : Returns a Message to Confirm the Update or Deny it.
     */
    @PostMapping("/updatePlayerPosition/{gameId}")
    public ResponseEntity<String> updatePlayerPosition(
            @PathVariable(value = "gameId") Long gameId,
            @RequestBody UpdatePlayerPositionDTO changes
    ) {
        var game = this.gameService.getGameById(gameId);
        var tile = this.tileService.getTileAt(game.getBoard(), changes.getX(), changes.getY());
        var player = this.playerService.getPlayersFor(gameId).stream()
                .filter(p -> p.getName().equals(changes.getPlayerName()))
                .findFirst().get();
        messengerService.sendMove(player.getId(), gameId);
        this.playerService.UpdateCurrentTile(player, tile);
        if (player.getCurrentTile() == tile) {
            return ResponseEntity.ok("Updated");
        }
        return new ResponseEntity<>("Not Updated", HttpStatus.NOT_MODIFIED);
    }
}
