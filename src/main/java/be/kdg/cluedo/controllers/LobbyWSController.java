package be.kdg.cluedo.controllers;

import be.kdg.cluedo.controllers.dtos.websocketDTOs.LobbyCreateDTO;
import be.kdg.cluedo.controllers.dtos.websocketDTOs.LobbyStartGameDTO;
import be.kdg.cluedo.controllers.dtos.websocketDTOs.PlayerJoinDTO;
import be.kdg.cluedo.controllers.dtos.websocketDTOs.PlayerUpdateDTO;
import be.kdg.cluedo.domain.websocket.LobbyPlayer;
import be.kdg.cluedo.domain.websocket.RandomString;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Slf4j
@Controller
public class LobbyWSController {
    HashMap<String, List<LobbyPlayer>> allPlayers;
    LobbyWSController(){
        allPlayers = new HashMap<>();
    }

    @MessageMapping("/lobby/create")
    @SendTo("/lobby/created")
    public LobbyCreateDTO createLobby(String id) {
        // create lobbyID
        String lobbyId = new RandomString(8).nextString().toUpperCase();

        allPlayers.put(lobbyId,new ArrayList<>());
        log.info(lobbyId+" "+id);
        return new LobbyCreateDTO(id,lobbyId);
    }

    @MessageMapping("/lobby/playerjoin")
    @SendTo("/lobby/playerjoined")
    public HashMap<String, List<LobbyPlayer>> playerJoin( @Payload PlayerJoinDTO playerJoinDTO) {

        log.info(playerJoinDTO.getLobbyId()+" "+playerJoinDTO.getName());

        var lobbyPlayers = allPlayers.get(playerJoinDTO.getLobbyId());
        if(!playerJoinDTO.getName().equals("init"))
            lobbyPlayers.add(new LobbyPlayer(playerJoinDTO.getName()));

        var returnValues = new HashMap<String,List<LobbyPlayer>>();
        returnValues.put(playerJoinDTO.getLobbyId(),lobbyPlayers);
        return returnValues;
    }

    @MessageMapping("/lobby/gamestart")
    @SendTo("/lobby/gamestarted")
    public LobbyStartGameDTO playerJoin(@Payload LobbyStartGameDTO dto) {
        allPlayers.remove(dto.getLobbyId());
        return dto;
    }

}