package be.kdg.cluedo.controllers.dtos.websocketDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerWonDTO {
    private int gameId;
    private String player;
}
