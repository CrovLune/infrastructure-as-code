package be.kdg.cluedo.controllers.dtos;

import be.kdg.cluedo.controllers.dtos.player.PlayerDTO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class GameEndDTO {
    private List<PlayerDTO> players;
    private long gameID;
}