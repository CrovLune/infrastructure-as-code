package be.kdg.cluedo.controllers.dtos.player;

import be.kdg.cluedo.domain.tiles.Tile;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
/**
 * PlayerPositionDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Zelenskiy Aleksey
 * */
public class PlayerPositionDTO {
    private String playerName;
    private Tile tile;
}
