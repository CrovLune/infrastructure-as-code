package be.kdg.cluedo.controllers.dtos;

import be.kdg.cluedo.controllers.dtos.player.PlayerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
/**
 * GameDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Zelenskiy Aleksey
 * */
public class GameDTO {
    private List<PlayerDTO> players;
    private String boardName;
}
