package be.kdg.cluedo.controllers.dtos.websocketDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerUpdateDTO {
    private int gameId;
    private int x;
    private int y;
}
