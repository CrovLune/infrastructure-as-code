package be.kdg.cluedo.controllers.dtos.suggestion;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiSuggestion;
import be.kdg.cluedo.domain.players.Suggestion;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
/**
 * AccusationDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Zelenskiy Aleksey
 * */
public class AccusationDTO {
    private RiSuggestion suggestion;
    private Long gameId;
    private Long playerId;
}
