package be.kdg.cluedo.controllers.dtos;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiValidTile;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
/**
 * DiceRollDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Zelenskiy Aleksey
 * */
public class DiceRollDTO {
    private int eyes;
    private Set<RiValidTile> availableTiles;
}
