package be.kdg.cluedo.controllers.dtos.player;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
/**
 * PlayerPositionDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Baguette Jordi
 * */
public class UpdatePlayerPositionDTO {
    private String playerName;
    private int x;
    private int y;
}
