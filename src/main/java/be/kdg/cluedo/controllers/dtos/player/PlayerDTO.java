package be.kdg.cluedo.controllers.dtos.player;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
/**
 * PlayerDTO is created to exchange only the necessary data between Front-End & Back-End.
 *
 * @Author : Zelenskiy Aleksey
 * */
public class PlayerDTO {
    private String name;
    private String chosenCharacter;
}
