package be.kdg.cluedo.controllers.dtos.websocketDTOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerJoinDTO {
    private String lobbyId;
    private String name;
}
