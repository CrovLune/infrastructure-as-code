package be.kdg.cluedo.controllers.dtos.relevantinfo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RiValidTile {
    private int x;
    private int y;
}
