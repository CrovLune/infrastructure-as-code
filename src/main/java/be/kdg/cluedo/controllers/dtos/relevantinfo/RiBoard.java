package be.kdg.cluedo.controllers.dtos.relevantinfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class RiBoard {
    private long id;
    private String name;
    private List<Long> areas;
    private List<Long> startingPositions;

    public RiBoard() {
        this.areas = new ArrayList<>();
        this.startingPositions = new ArrayList<>();
    }
}
