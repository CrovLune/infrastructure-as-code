package be.kdg.cluedo.controllers.dtos.websocketDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LobbyCreateDTO {
    private String id;
    private String lobbyId;
}
