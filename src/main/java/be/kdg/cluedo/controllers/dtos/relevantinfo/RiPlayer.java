package be.kdg.cluedo.controllers.dtos.relevantinfo;

import be.kdg.cluedo.domain.cards.types.CharacterType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
public class RiPlayer {
    private long id;
    private String name;
    private CharacterType chosenCharacter;
    private long gameId;
    private int x;
    private int y;
    private Set<Long> cards;

    public RiPlayer() {
        this.cards = new HashSet<>();
    }
}
