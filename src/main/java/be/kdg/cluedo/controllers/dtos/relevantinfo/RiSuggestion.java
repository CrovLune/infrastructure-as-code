package be.kdg.cluedo.controllers.dtos.relevantinfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RiSuggestion {
    private long weaponCardId;
    private long roomCardId;
    private long characterCardId;
}
