package be.kdg.cluedo.controllers.dtos.websocketDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LobbyStartGameDTO {
    private String gameId;
    private String lobbyId;
}
