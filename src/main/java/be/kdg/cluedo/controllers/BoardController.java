package be.kdg.cluedo.controllers;


import be.kdg.cluedo.controllers.dtos.relevantinfo.RiBoard;
import be.kdg.cluedo.services.BoardService;
import be.kdg.cluedo.services.GameService;
import be.kdg.cluedo.services.restServices.BoardRestService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/game-service")
public class BoardController {
    private final BoardRestService boardRestService;
    private final GameService gameService;
    private final BoardService boardService;

    /**
     * Get a list of board names.
     *
     * @Author : Zelenskiy Aleksey
     * @Return : ResponseEntity<List<String>> Will return a List of Board names.
     */
    @GetMapping("/getAllBoards")
    public ResponseEntity<List<String>> getAllBoards() {
        var boards = this.boardRestService.getAllBoards();

        if (!boards.isEmpty()) {
            return ResponseEntity.ok(boards);
        }

        return ResponseEntity.notFound().build();
    }

    /**
     * Gets the Board info from a given Game ID.
     *
     * @Author : Zelenskiy Aleksey
     * @Param : gameId.
     * @Return : ResponseEntity<RiBoard> This returns a short version of Board, that contains only ID's instead of Objects.
     * @See RiBoard
     */
    @GetMapping("/getBoard/{gameId}")
    public ResponseEntity<RiBoard> getBoard(@PathVariable(value = "gameId") Long gameId) {
        var game = this.gameService.getGameById(gameId);
        var board = game.getBoard();

        return ResponseEntity.ok(this.boardService.convertToRiBoard(board));
    }
}
