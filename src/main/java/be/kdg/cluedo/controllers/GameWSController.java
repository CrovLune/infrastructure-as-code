package be.kdg.cluedo.controllers;


import be.kdg.cluedo.controllers.dtos.websocketDTOs.PlayerDiedDTO;
import be.kdg.cluedo.controllers.dtos.websocketDTOs.PlayerUpdateDTO;
import be.kdg.cluedo.controllers.dtos.websocketDTOs.PlayerWonDTO;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.services.MessengerService;
import be.kdg.cluedo.services.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class GameWSController {
    private final PlayerService playerService;
    private final MessengerService messengerService;

    public GameWSController(PlayerService playerService, MessengerService messengerService) {
        this.playerService = playerService;
        this.messengerService = messengerService;
    }

    @MessageMapping("/game/playerupdate")
    @SendTo("/game/playerupdated")
    public PlayerUpdateDTO playerUpdate(PlayerUpdateDTO dto) {
        log.info("playerUpdate:" + dto.getGameId() +" to (" + dto.getX() +", "+dto.getY() +")");
        return dto;
    }

    @MessageMapping("/game/nextplayer")
    @SendTo("/game/nextplayered")
    public String nextPlayer(String gameId) {
        log.info("nextPlayer: " + gameId);
        return gameId;
    }

    @MessageMapping("/game/win")
    @SendTo("/game/won")
    public String gameWin(String player) {
        log.info("player won: " + player);
        return player;
    }

    @MessageMapping("/game/playerdied")
    @SendTo("/game/playerdead")
    public PlayerDiedDTO winGame(PlayerDiedDTO dto) {
        log.info("game won: " + dto.getPlayer());
        return dto;
    }

    @MessageMapping("/game/gamewin")
    @SendTo("/game/gamewon")
    public PlayerWonDTO winGame(PlayerWonDTO dto) {
        log.info("game won: " + dto.getPlayer());
        var players = playerService.getPlayersFor((long) dto.getGameId());
        for (Player p :
                players) {
            if (p.getName().equals(dto.getPlayer())) {
                messengerService.sendEndWonUserSession((int) p.getId(), dto.getGameId());
            } else {
                messengerService.sendEndUserSession((int) p.getId(), dto.getGameId());
            }
        }
        messengerService.endGame(dto.getGameId());
        return dto;
    }
}
