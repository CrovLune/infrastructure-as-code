package be.kdg.cluedo.controllers;

import be.kdg.cluedo.domain.clues.Clue;
import be.kdg.cluedo.services.ClueService;
import be.kdg.cluedo.services.PlayerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/game-service")
@AllArgsConstructor

public class ClueController {
    private final ClueService clueService;
    private final PlayerService playerService;

    /**
     * Get Clues of a particular player
     *
     * @param playerId The id of the player which needs his clues.
     * @return ResponseEntity<Set < Clue>> Will return a Set of Clues for the playerID.
     * @author Zelenskiy Aleksey
     */
    @Operation(summary = "Get Clues of a particular player")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Clues",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Clue.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Clues not found",
                    content = @Content)
    })
    @GetMapping("/getClues/{playerId}")
    @ResponseStatus
    public ResponseEntity<Set<Clue>> getClues(@PathVariable(value = "playerId") Long playerId) {
        var player = this.playerService.getPlayerBy(playerId);
        var clues = clueService.getCluesOfPlayer(player);

        if (clues == null) {
            return new ResponseEntity<>(HttpStatus.valueOf(500));
        }
        if (clues.size() == 0) {
            return new ResponseEntity<>(HttpStatus.valueOf(202));
        }
        return new ResponseEntity<>(clues, HttpStatus.OK);
    }

    /**
     * Apply Changes to a particular Clue.
     *
     * @param clue The clue that needs an update.
     * @return ResponseEntity<HttpStatus> Returns an OK status after the clue is changed.
     * @author Zelenskiy Aleksey
     */
    @Operation(summary = "Apply Changes to a particular Clue.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Clue Updated",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = HttpStatus.class))})
    })
    @PostMapping("/changeClue/")
    @ResponseStatus
    public ResponseEntity<Void> changeClue(@RequestBody Clue clue) {
        this.clueService.changeClue(clue);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
