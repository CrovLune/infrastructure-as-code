package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameEndDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GameEndMessenger {
    @Value("${gameEndQueue}")
    private String gameEndQueue;
    private final RabbitTemplate rabbitTemplate;

    public GameEndMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendEndOfGame(GameEndDTO gameEndDTO) {
        try {
            log.info(gameEndDTO.toString());
            rabbitTemplate.convertAndSend(gameEndQueue,gameEndDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + gameEndDTO.toString() );
        }
    }
}
