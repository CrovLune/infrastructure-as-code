package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameEndDTO;
import be.kdg.cluedo.messaging.dto.GameStartDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GameStartMessenger {
    @Value("${gameStartQueue}")
    private String gameStartQueue;
    private final RabbitTemplate rabbitTemplate;

    public GameStartMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendStartOfGame(GameStartDTO gameStartDTO) {
        try {
            log.info(gameStartDTO.toString());
            rabbitTemplate.convertAndSend(gameStartQueue,gameStartDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + gameStartDTO.toString() );
        }
    }
}
