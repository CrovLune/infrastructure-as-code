package be.kdg.cluedo.messaging.dto;

import lombok.Data;

@Data
public class MoveDTO {
    private int gameID;
    private int userID;
}
