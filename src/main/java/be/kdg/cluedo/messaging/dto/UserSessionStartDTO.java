package be.kdg.cluedo.messaging.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserSessionStartDTO {
    private int userID;
    private int gameID;
    private LocalDateTime sessionStartDateTime;
}
