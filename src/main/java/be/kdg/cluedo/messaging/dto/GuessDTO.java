package be.kdg.cluedo.messaging.dto;

import lombok.Data;

@Data
public class GuessDTO {
    private int gameID;
    private int userID;
    private String room;
    private String weapon;
    private String character;
}
