package be.kdg.cluedo.messaging.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GameStartDTO {
    private int gameID;
    private LocalDateTime gameStartDateTime;
}
