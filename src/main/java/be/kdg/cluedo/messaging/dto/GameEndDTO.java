package be.kdg.cluedo.messaging.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GameEndDTO {
    private int gameID;
    private LocalDateTime gameEndDateTime;
}
