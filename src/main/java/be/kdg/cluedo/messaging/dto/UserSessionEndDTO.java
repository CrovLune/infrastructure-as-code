package be.kdg.cluedo.messaging.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserSessionEndDTO {
    private int userID;
    private int gameID;
    private LocalDateTime sessionEndDateTime;
    private Boolean won;
}
