package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameStartDTO;
import be.kdg.cluedo.messaging.dto.GuessDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GuessMessenger {
    @Value("${guessQueue}")
    private String guessQueue;
    private final RabbitTemplate rabbitTemplate;

    public GuessMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendGuess(GuessDTO guessDTO) {
        try {
            log.info(guessDTO.toString());
            rabbitTemplate.convertAndSend(guessQueue, guessDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + guessDTO.toString() );
        }
    }
}
