package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameStartDTO;
import be.kdg.cluedo.messaging.dto.UserSessionStartDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SessionStartMessenger {
    @Value("${sessionStartQueue}")
    private String sessionStartQueue;
    private final RabbitTemplate rabbitTemplate;

    public SessionStartMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendStartOfUserSession(UserSessionStartDTO userSessionStartDTO) {
        try {
            log.info(userSessionStartDTO.toString());
            rabbitTemplate.convertAndSend(sessionStartQueue, userSessionStartDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + userSessionStartDTO.toString() );
        }
    }
}
