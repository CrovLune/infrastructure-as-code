package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameStartDTO;
import be.kdg.cluedo.messaging.dto.UserSessionEndDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SessionEndMessenger {
    @Value("${sessionEndQueue}")
    private String sessionEndQueue;
    private final RabbitTemplate rabbitTemplate;

    public SessionEndMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendEndOfUserSession(UserSessionEndDTO userSessionEndDTO) {
        try {
            log.info(userSessionEndDTO.toString());
            rabbitTemplate.convertAndSend(sessionEndQueue, userSessionEndDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + userSessionEndDTO.toString() );
        }
    }
}
