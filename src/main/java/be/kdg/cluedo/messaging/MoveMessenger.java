package be.kdg.cluedo.messaging;

import be.kdg.cluedo.messaging.dto.GameStartDTO;
import be.kdg.cluedo.messaging.dto.MoveDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MoveMessenger {
    @Value("${moveQueue}")
    private String moveQueue;
    private final RabbitTemplate rabbitTemplate;

    public MoveMessenger(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMove(MoveDTO moveDTO) {
        try {
            log.info(moveDTO.toString());
            rabbitTemplate.convertAndSend(moveQueue, moveDTO);
        }catch (Exception ex) {
            log.error(ex.toString() + " met dto: " + moveDTO.toString() );
        }
    }
}
