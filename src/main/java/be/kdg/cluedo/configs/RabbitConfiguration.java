package be.kdg.cluedo.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {
    @Value("${gameEndQueue}")
    private String gameEndQueue;
    @Value("${gameStartQueue}")
    private String gameStartQueue;
    @Value("${guessQueue}")
    private String guessQueue;
    @Value("${moveQueue}")
    private String moveQueue;
    @Value("${sessionEndQueue}")
    private String sessionEndQueue;
    @Value("${sessionStartQueue}")
    private String sessionStartQueue;

    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public Queue gameEndQueue() {
        return new Queue(gameEndQueue, false);
    }
    @Bean
    public Queue gameStartQueue() {
        return new Queue(gameStartQueue, false);
    }
    @Bean
    public Queue guessQueue() {
        return new Queue(guessQueue, false);
    }
    @Bean
    public Queue moveQueue() {
        return new Queue(moveQueue, false);
    }
    @Bean
    public Queue sessionEndQueue() {
        return new Queue(sessionEndQueue, false);
    }
    @Bean
    public Queue sessionStartQueue() {
        return new Queue(sessionStartQueue, false);
    }

}
