package be.kdg.cluedo.factories;

import be.kdg.cluedo.domain.Area;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Position;
import be.kdg.cluedo.domain.tiles.Tile;
import be.kdg.cluedo.domain.tiles.TileType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import org.json.*;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.Scanner;

@Component
@Slf4j
@AllArgsConstructor
@Transactional
public class StandardBoardFactory implements Factory {

    @Override
    public Board createBoard(String name, String jsonInput) {
        //? Board
        var board = new Board();

        Scanner mapScanner = null;

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(jsonInput);

        if (is != null) {
            mapScanner = new Scanner(is).useDelimiter("\\A");
        } else {
            log.error("InputStream is NULL");
        }

        assert mapScanner != null;
        JSONObject mapJson = new JSONObject(mapScanner.next());

        JSONArray mapJsonArray = mapJson.getJSONArray("rooms");
        for (int ry = 0; ry < mapJsonArray.length(); ry++) { //rooms y
            for (int rx = 0; rx < mapJsonArray.getJSONArray(ry).length(); rx++) { //rooms x
                JSONObject roomJson = mapJsonArray.getJSONArray(ry).getJSONObject(rx);
                String areaName = roomJson.getString("type");

                //? Area + Link to Board
                var area = new Area(board, areaName);

                JSONArray tiles = roomJson.getJSONArray("tiles");
                for (int y = 0; y < tiles.length(); y++) { //room tiles x
                    for (int x = 0; x < tiles.getJSONArray(y).length(); x++) { //room tiles y
                        int tileType = tiles.getJSONArray(y).getInt(x);

                        //? Tile + Link to Area
                        var tile = new Tile(area, 1, TileType.tileTypeById(tileType));

                        //? Position + Link to Tile
                        var position = new Position(tile, (rx * 5) + x, (ry * 5) + y); //absolute coordinaat van tile (niet relatief per room)

                        //? Tile Link to Position
                        tile.setPosition(position);

                        //? Area Link to Tile
                        area.getTiles().add(tile);
                    }
                }
                //? Board Link to Area
                board.getAreas().add(area);
            }
        }
        board.setName(name);

        return board;
    }
}
