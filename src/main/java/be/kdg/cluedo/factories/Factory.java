package be.kdg.cluedo.factories;

import be.kdg.cluedo.domain.Board;

public interface Factory {
    Board createBoard(String name, String jsonInput);
}
