package be.kdg.cluedo.factories;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Position;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name="startpos")
@NoArgsConstructor
public class StartingPosition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "board_id")
    private Board board;

    private String characterName;

    @OneToOne(fetch = FetchType.LAZY)
    private Position position;


    public StartingPosition(Board board, String characterName, Position position) {
        this.board = board;
        this.characterName = characterName;
        this.position = position;
    }
}
