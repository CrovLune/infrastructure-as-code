package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.domain.tiles.Tile;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import static be.kdg.cluedo.domain.Direction.*;
import static be.kdg.cluedo.domain.cards.types.RoomType.*;
import static be.kdg.cluedo.domain.tiles.TileType.PORTAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class TileServiceTest {
    private Board board;
    @Autowired
    TileService service;
    @Autowired
    GameService gameService;
    @Autowired
    PlayerService playerService;
    List<Player> players;

    @BeforeEach
    void init() {
        this.board = gameService.getAllGames().get(0).getBoard();
        this.players = playerService.getPlayersFor((long) 1);
    }


    @Test
    void getTileAt() {
        //* x > 14 & y > 14 = OUT OF BOUNDS
        assertThat(service.getTileAt(this.board, 1, 1)).isNotNull();
        assertThat(service.getTileAt(this.board, 16, 13)).isNull();
    }

    @Test
    void getTilesAt() {
        System.out.println(service.getTilesAt(this.board, STUDY));
        var tiles = service.getTilesAt(this.board, STUDY);
        for (var tile : tiles) {
            System.out.println(String.format("POS[%s]-TYPE[%s]-AREA[%s]", tile.getPosition(), tile.getType(), tile.getArea()));
        }
        assertThat(service.getTilesAt(this.board, STUDY)).isNotNull();
    }

    @Test
    void getPortalTile() {
        //* Kitchen has no Portal -> will be return NoSuchElementException
        assertThrows(NoSuchElementException.class, () -> service.getPortalTile(this.board, KITCHEN));

        var lounge = service.getPortalTile(this.board, LOUNGE);
        assertThat(lounge.getType()).isEqualTo(PORTAL);
    }

    @Test
    void getAllTilesOf() {
        //* In case it's a StandardBoard -> 225 total tiles
        assertThat(service.getAllTilesOf(this.board).size()).isEqualTo(225);
    }

    @Test
    void getTileInDirection() {
        var tile = service.getTileAt(this.board, 2, 6);
        assertThat(service.getTileInDirection(this.board, tile, UP)).isNotNull();
        assertThat(service.getTileInDirection(this.board, tile, RIGHT)).isNotNull();
        assertThat(service.getTileInDirection(this.board, tile, DOWN)).isNotNull();
        assertThat(service.getTileInDirection(this.board, tile, LEFT)).isNotNull();
    }

    @Test
    void getTilesAroundPORTAL() {
        //* Tile is near PORTAL -> should give tiles further away
        var tile2 = service.getTileAt(this.board, 2, 1);
        var tiles2 = service.getTilesAround(this.board, tile2, 4, this.players);
        assertThat(tiles2).isNotNull();
        assertThat(tiles2.size()).isEqualTo(19);
        //* Position(4,14) -> Portal Tile
        assertThat(tiles2.stream().map(Tile::getPosition).anyMatch(pos -> pos.getX() == 2 && pos.getY() == 14)).isTrue();
    }


    @Test
    void getTilesAroundDice() {
        var tile = service.getTileAt(this.board, 2, 1);

        var tiles1 = service.getTilesAround(this.board, tile, 1, this.players);
        assertThat(tiles1).isNotNull();
        assertThat(tiles1.size()).isEqualTo(3);

        var tiles2 = service.getTilesAround(this.board, tile, 2, this.players);
        assertThat(tiles2).isNotNull();
        assertThat(tiles2.size()).isEqualTo(8);


        var tiles3 = service.getTilesAround(this.board, tile, 3, this.players);
        assertThat(tiles3).isNotNull();
        assertThat(tiles3.size()).isEqualTo(13);


        var tiles4 = service.getTilesAround(this.board, tile, 4, this.players);
        assertThat(tiles4).isNotNull();
        assertThat(tiles4.size()).isEqualTo(19);


        var tiles5 = service.getTilesAround(this.board, tile, 5, this.players);
        assertThat(tiles5).isNotNull();
        assertThat(tiles5.size()).isEqualTo(25);


        var tiles6 = service.getTilesAround(this.board, tile, 6, this.players);
        assertThat(tiles6).isNotNull();
        assertThat(tiles6.size()).isEqualTo(36);
    }

    @Test
    void testRandomDiceThrowWithRandomTile() {
        var random = new Random();
        var dice = random.nextInt(6) + 1;

        var t = service.getAllAvailableTiles(this.board);
//        var tile = t.get(random.nextInt(t.size()));
        var tile = service.getTileAt(board, 14, 12);

        var tiles = service.getTilesAround(this.board, tile, 2, this.players);
        assertThat(tiles).isNotNull();
        assertThat(tiles.size()).isBetween(1, 39);
    }

    @Test
    void getPortalTileDestination() {
        var random = new Random();
        var t = service.getPortalTiles(this.board);
        var tile = t.get(random.nextInt(t.size()));
        var a = service.getPortalTileDestination(this.board, tile);

        assertThat(a).isNotNull();
        assertThat(a.getType()).isEqualTo(PORTAL);
        assertThat(a.getArea().getName()).isIn(BALLROOM.toString(), BILLIARD_ROOM.toString(), LOUNGE.toString());
    }
}