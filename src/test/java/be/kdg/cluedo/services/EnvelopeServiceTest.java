package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Game;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class EnvelopeServiceTest {
    private Game game;
    @Autowired
    GameService gameService;
    @Autowired
    EnvelopeService envelopeService;

    @BeforeEach
    void init() {
        this.game = gameService.getAllGames().get(0);
    }

    @Test
    void getEnvelopeFor() {
        var env = this.envelopeService.getEnvelopeFor(this.game);

        assertThat(env).isNotNull();
        assertThat(env.getGame()).isEqualTo(this.game);
    }
}