package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.Character;
import be.kdg.cluedo.domain.cards.Room;
import be.kdg.cluedo.domain.cards.Weapon;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.players.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class CardServiceTest {
    private Game game;
    @Autowired
    CardService service;
    @Autowired
    GameService gameService;
    @Autowired
    EnvelopeService envelopeService;
    @Autowired
    PlayerService playerService;

    @BeforeEach
    void init() {
        this.game = gameService.getAllGames().get(0);
    }

    @Test
    void getWeaponCards() {
        var weapons = this.service.getWeaponCards(this.game);
        assertThat(weapons).isNotNull();
        assertThat(weapons.get(0)).isNotNull();
        assertThat(weapons.get(0).getClass()).isEqualTo(Weapon.class);
    }

    @Test
    void getCharacterCards() {
        var characters = this.service.getCharacterCards(this.game);
        assertThat(characters).isNotNull();
        assertThat(characters.get(0)).isNotNull();
        assertThat(characters.get(0).getClass()).isEqualTo(Character.class);
    }

    @Test
    void getRoomCards() {
        var rooms = this.service.getRoomCards(this.game);
        assertThat(rooms).isNotNull();
        assertThat(rooms.get(0)).isNotNull();
        assertThat(rooms.get(0).getClass()).isEqualTo(Room.class);
    }

    @Test
    void GetWeaponCardsFromList() {
        var list = new ArrayList<>(this.game.getCards());
        var rooms = this.service.getRoomCards(list);
        assertThat(rooms).isNotNull();
        assertThat(rooms.get(0)).isNotNull();
        assertThat(rooms.get(0).getClass()).isEqualTo(Room.class);
    }

    @Test
    void GetCharacterCardsFromList() {
        var list = new ArrayList<>(this.game.getCards());
        var characters = this.service.getCharacterCards(list);
        assertThat(characters).isNotNull();
        assertThat(characters.get(0)).isNotNull();
        assertThat(characters.get(0).getClass()).isEqualTo(Character.class);
    }

    @Test
    void GetRoomCardsFromList() {
        var list = new ArrayList<>(this.game.getCards());
        var rooms = this.service.getRoomCards(list);
        assertThat(rooms).isNotNull();
        assertThat(rooms.get(0)).isNotNull();
        assertThat(rooms.get(0).getClass()).isEqualTo(Room.class);
    }

    @Test
    void putCardsInGame() {
        this.service.putCardsInGame(this.game);
        var env = this.envelopeService.getEnvelopeFor(game);

        assertThat(this.game.getCards()).isNotNull();
        //* 24 total cards
        assertThat(this.game.getCards().size()).isEqualTo(22);
        assertThat(env).isNotNull();
        assertThat(env.getGame()).isEqualTo(game);
    }

    @Test
    void spreadCardsOverPlayers() {
        for (int i = 0; i < 7; i++) {
            var player = new Player("Jeff" + i, CharacterType.values()[i], this.game);
            this.playerService.save(player);
        }

        this.service.spreadCardsOverPlayers(this.game);


        var players = this.playerService.getPlayersFor(game);
        for (var player : players) {
            assertThat(player.getCards()).isNotNull();
            assertThat(player.getCards().size()).isGreaterThan(0);
        }
    }
}