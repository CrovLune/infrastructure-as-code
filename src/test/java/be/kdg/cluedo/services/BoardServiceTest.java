package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class BoardServiceTest {
    private Board board;
    @Autowired
    BoardService boardService;
    @Autowired
    GameService gameService;

    @BeforeEach
    void init() {
        this.board = gameService.getAllGames().get(0).getBoard();
    }

    @Test
    void getBoardByName() {
        var board = this.boardService.getBoardBy("StandardBoard");
        assertThat(board).isNotNull();
        assertThat(board.getName()).isEqualTo("StandardBoard");
    }

    @Test
    void GetBoardByBoardId() {
        var id = boardService.getAllBoards().get(0).getId();
        var board = this.boardService.getBoardBy(id);
        assertThat(board).isNotNull();
        assertThat(board.getId()).isEqualTo(id);
    }

    @Test
    void GetBoardByContainsArea() {
        var randomArea = new ArrayList<>(this.board.getAreas()).get(0);
        var board = this.boardService.getBoardBy(randomArea);
        assertThat(board).isNotNull();
        assertThat(board.getAreas().size()).isEqualTo(9);
        var found = board.getAreas()
                .stream()
                .filter(x -> x.getName().equals(randomArea.getName()))
                .findFirst()
                .orElseThrow();
        assertThat(found).isNotNull();
    }
}