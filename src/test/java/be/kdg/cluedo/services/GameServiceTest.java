package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.relevantinfo.RiSuggestion;
import be.kdg.cluedo.controllers.dtos.suggestion.SuggestionDTO;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.players.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class GameServiceTest {
    private Game game;
    @Autowired
    GameService service;
    @Autowired
    EnvelopeService envelopeService;
    @Autowired
    CardService cardService;
    @Autowired
    PlayerService playerService;

    @BeforeEach
    void init() {
        this.game = this.service.getAllGames().get(0);
    }

    @Test
    void checkAccusation() {
        var weapoonCard = this.cardService.getWeaponCards(this.game).get(0);
        var characterCard = this.cardService.getCharacterCards(this.game).get(0);
        var roomCard = this.cardService.getRoomCards(this.game).get(0);
        var suggestion = new RiSuggestion(weapoonCard.getId(), roomCard.getId(), characterCard.getId());
        var envelope = this.envelopeService.getEnvelopeFor(this.game);

        var result = this.service.checkAccusation(suggestion, envelope);

        assertThat(result.getRight()).isNotNull();
        assertThat(result.getWrong()).isNotNull();
        assertThat(result.getRight().size()).isGreaterThanOrEqualTo(0);
        assertThat(result.getWrong().size()).isGreaterThanOrEqualTo(0);
    }

    @Test
    void checkSuggestion() {
        var weapoonCard = this.cardService.getWeaponCards(this.game).get(0);
        var characterCard = this.cardService.getCharacterCards(this.game).get(0);
        var roomCard = this.cardService.getRoomCards(this.game).get(0);

        for (int i = 0; i < 7; i++) {
            var player = new Player("Jeff" + i, CharacterType.values()[i], this.game);
            this.playerService.save(player);
        }
        this.service.save(this.game);

        this.cardService.spreadCardsOverPlayers(this.game);
        this.service.save(this.game);

        var players = this.playerService.getPlayersFor(game);
        var riSuggestion = new RiSuggestion(weapoonCard.getId(), roomCard.getId(), characterCard.getId());
        var suggestion = new SuggestionDTO(riSuggestion, players.get(1).getId(),players.get(0).getId(), game.getId());

        for (Player player : players) {
            System.out.println(player);
            var result = this.service.checkSuggestion(suggestion, player);
            assertThat(result).isNotNull();
        }
    }

    @Test
    void getGameById() {
        var g = this.service.getGameById(this.game.getId());

        assertThat(g).isEqualTo(this.game);
    }
}