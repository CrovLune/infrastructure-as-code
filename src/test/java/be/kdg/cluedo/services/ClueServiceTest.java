package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.clues.Clue;
import be.kdg.cluedo.domain.clues.State;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class ClueServiceTest {
    @Autowired
    ClueService clueService;
    @Autowired
    PlayerService playerService;
    @Autowired
    GameService gameService;
    @Autowired
    CardService cardService;

    @Test
    void changeClue() {
        var game = this.gameService.getAllGames().get(0);
        var players = this.playerService.getPlayersFor(game);
        var player = players.get(0);

        var card = this.cardService.getRoomCards(game).get(0);

        var clue = new Clue(player, State.QUESTION_MARK, card, players.get(1));
        this.clueService.save(clue);

        var incoming = new Clue(player, State.PRESENT, card, players.get(1));

        this.clueService.changeClue(incoming);

        var clues = this.clueService.getCluesOfPlayer(player);

        assertThat(new ArrayList<>(clues).get(0).getState()).isEqualTo(incoming.getState());

    }

}