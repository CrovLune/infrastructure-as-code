package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.players.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

import static be.kdg.cluedo.domain.cards.types.RoomType.KITCHEN;
import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class AreaServiceTest {
    private Board board;
    @Autowired
    AreaService service;
    @Autowired
    GameService gameService;

    @BeforeEach
    void init() {
        this.board = this.gameService.getAllGames().get(0).getBoard();
    }

    @Test
    void getAreaBy() {
        var area = this.service.getAreaBy(this.board, KITCHEN);
        assertThat(area).isNotNull();
        assertThat(area.getName()).isEqualTo(KITCHEN.toString());
    }

    @Test
    void testGetAreaBy() {
        var area = this.service.getAreaBy(this.board.getId(), KITCHEN);
        assertThat(area).isNotNull();
        assertThat(area.getName()).isEqualTo(KITCHEN.toString());
    }

    @Test
    void getAllAreasBy() {
        var areas = this.service.getAllAreasBy(this.board);
        assertThat(areas).isNotNull();
        assertThat(areas.size()).isEqualTo(9);
    }

    @Test
    void testGetAllAreasBy() {
        var areas = this.service.getAllAreasBy(this.board.getId());
        assertThat(areas).isNotNull();
        assertThat(areas.size()).isEqualTo(9);
    }
}