package be.kdg.cluedo.services;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class PositionServiceTest {
    private Board board;
    @Autowired
    PositionService service;
    @Autowired
    GameService gameService;

    @BeforeEach
    void init() {
        this.board = gameService.getAllGames().get(0).getBoard();
    }

    @Test
    void getPositionAtBoard() {
        assertThat(service.getPositionAt(this.board, 1, 1)).isNotNull();
        assertThat(service.getPositionAt(this.board, 15, 61)).isNull();
    }

    @Test
    void getPositionAtBoardId() {
        assertThat(service.getPositionAt(this.board.getId(), 1, 1)).isNotNull();
        assertThat(service.getPositionAt(this.board.getId(), 15, 61)).isNull();
    }

}