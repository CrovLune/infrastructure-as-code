package be.kdg.cluedo.services;

import be.kdg.cluedo.controllers.dtos.player.PlayerDTO;
import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.cards.types.CharacterType;
import be.kdg.cluedo.domain.players.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class PlayerServiceTest {
    private Game game;
    private Player player;
    private Board board;
    @Autowired
    PlayerService service;
    @Autowired
    GameService gameService;
    @Autowired
    TileService tileService;

    @BeforeEach
    void init() {
        this.board = this.gameService.getAllGames().get(0).getBoard();
        this.player = this.service.getAllPlayers().get(0);
        this.game = this.gameService.getAllGames().get(0);
    }

    @Test
    void getAllPlayers() {
        var players = this.service.getAllPlayers();

        assertThat(players).isNotNull();
    }

    @Test
    void getPlayerBy() {
        var player = this.service.getPlayerBy(this.player.getId());

        assertThat(player).isNotNull();
        assertThat(player).isEqualTo(this.player);
    }

    @Test
    void updateCurrentTile() {
        var tile = this.tileService.getAllTilesOf(board).get(0);
        var tile2 = this.tileService.getAllTilesOf(board).get(1);
        player.setCurrentTile(tile);
        var oldTile = player.getCurrentTile();

        this.service.UpdateCurrentTile(player,tile2);

        assertThat(oldTile).isNotEqualTo(player.getCurrentTile());
    }

    @Test
    void createPlayersFor() {
        List<PlayerDTO> dtos = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            var dto = new PlayerDTO("test", CharacterType.values()[0].name());
            dtos.add(dto);
        }
        this.service.createPlayersFor(dtos,this.game);
        var players = this.service.getPlayersFor(this.game);

        assertThat(player).isNotNull();
        assertThat(players.size()).isGreaterThan(0);
    }
}