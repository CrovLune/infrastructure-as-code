package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Game;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("dev")
@SpringBootTest

@Slf4j
@Transactional
@Rollback
class GamesRepoTest {
    private Game game;
    @Autowired
    GamesRepo gamesRepo;

    @BeforeEach
    void init() {
        this.game = this.gamesRepo.findAll().get(0);
    }

    @Test
    void readGameById() {
        var g = this.gamesRepo.readGameById(game.getId());

        assertThat(g).isNotNull();
        assertThat(g).isEqualTo(this.game);
    }
}