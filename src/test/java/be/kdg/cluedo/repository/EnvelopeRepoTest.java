package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Game;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("dev")
@SpringBootTest

@Slf4j
@Transactional
@Rollback
class EnvelopeRepoTest {
    private Game game;
    @Autowired
    GamesRepo gamesRepo;
    @Autowired
    EnvelopeRepo repo;

    @BeforeEach
    void init() {
        this.game = this.gamesRepo.findAll().get(0);
    }

    @Test
    void readAllByGame() {
        var env = repo.readEnvelopeByGame(game);

        assertThat(env).isNotNull();
        assertThat(env.getGame()).isEqualTo(game);
    }

    @Test
    void getEnvelopeFor() {
        var env = repo.readEnvelopeByGame_Id(game.getId());
        assertThat(env).isNotNull();
        assertThat(env.getGame()).isEqualTo(game);
    }
}