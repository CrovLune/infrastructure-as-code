package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static be.kdg.cluedo.domain.cards.types.RoomType.KITCHEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("jpa")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class AreaRepoTest {
    private Board board;
    @Autowired
    GamesRepo gamesRepo;
    @Autowired
    AreaRepo repo;

    @BeforeEach
    void init() {
        this.board = this.gamesRepo.findAll().get(0).getBoard();
    }

    @Test
    void readAreaByBoardAndName() {
        var area = this.repo.readAreaByBoardAndName(this.board, KITCHEN.toString());
        assertThat(area).isNotNull();
        assertThat(area.getName()).isEqualTo(KITCHEN.toString());
    }

    @Test
    void readAreaByBoard_IdAndName() {
        var area = this.repo.readAreaByBoard_IdAndName(this.board.getId(), KITCHEN.toString());
        assertThat(area).isNotNull();
        assertThat(area.getName()).isEqualTo(KITCHEN.toString());
    }

    @Test
    void readAllByBoard() {
        var areas = this.repo.readAllByBoard(this.board);
        assertThat(areas).isNotNull();
        //* in case of Standard map it should be -> 9 Areas
        assertThat(areas.size()).isEqualTo(9);
    }

    @Test
    void readAllByBoard_Id() {
        var areas = this.repo.readAllByBoard_Id(this.board.getId());
        assertThat(areas).isNotNull();
        //* in case of Standard map it should be -> 9 Areas
        assertThat(areas.size()).isEqualTo(9);
    }
}