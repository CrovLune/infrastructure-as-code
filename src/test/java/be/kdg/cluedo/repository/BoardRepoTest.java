package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("jpa")
@SpringBootTest
@Slf4j
@Transactional
@Rollback
class BoardRepoTest {
    private Board board;
    @Autowired
    BoardRepo boardRepo;
    @Autowired
    AreaRepo areaRepo;

    @BeforeEach
    void init() {
        this.board = boardRepo.findAll().get(0);
    }

    @Test
    void readBoardByAreasContains() {
        var randomArea = areaRepo.readAllByBoard(this.board).get(0);
        var board = this.boardRepo.readBoardByAreasIsContaining(randomArea);
        assertThat(board).isNotNull();
        assertThat(board.getAreas().contains(randomArea)).isTrue();
    }

    @Test
    void readBoardByName() {
        var board = this.boardRepo.readBoardByName("StandardBoard");
        assertThat(board).isNotNull();
        assertThat(board.getName()).isEqualTo("StandardBoard");
    }

    @Test
    void readBoardById() {
        var id = boardRepo.findAll().get(0).getId();

        var board = this.boardRepo.readBoardById(id);
        assertThat(board).isNotNull();
        assertThat(board.getId()).isEqualTo(id);
    }


}