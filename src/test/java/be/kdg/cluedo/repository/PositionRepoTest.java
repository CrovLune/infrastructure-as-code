package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("dev")
@SpringBootTest

@Slf4j
@Transactional
@Rollback
class PositionRepoTest {
    private Board board;
    @Autowired
    GamesRepo gamesRepo;
    @Autowired
    PositionRepo positionRepo;

    @BeforeEach
    void init() {
        this.board = this.gamesRepo.findAll().get(0).getBoard();
    }


    //* Test to see if Repo returns a specific Position from a board.
    @Test
    void readPositionByTile_Area_Board_IdAndXAndY() {
        var position = this.positionRepo.readPositionByTile_Area_Board_IdAndXAndY(this.board.getId(), 2, 13);
        var outOfBounds = this.positionRepo.readPositionByTile_Area_Board_IdAndXAndY(this.board.getId(), 15, 20);

        assertThat(position).isNotNull();
        assertThat(outOfBounds).isNull();
    }


}