package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import be.kdg.cluedo.domain.cards.Room;
import be.kdg.cluedo.domain.cards.Weapon;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("dev")
@SpringBootTest

@Slf4j
@Transactional
@Rollback
class CardsRepoTest {
    private Board board;
    @Autowired
    GamesRepo gamesRepo;
    @Autowired
    CardsRepo cardsRepo;


    @BeforeEach
    void init() {
        this.board = this.gamesRepo.findAll().get(0).getBoard();
    }

    @Test
    void readCardsByBoardsContainingAndType() {
        var characters = this.cardsRepo.readCardsByBoardsContainingAndType(this.board, Character.class.getSimpleName());
        var rooms = this.cardsRepo.readCardsByBoardsContainingAndType(this.board, Room.class.getSimpleName());
        var weapons = this.cardsRepo.readCardsByBoardsContainingAndType(this.board, Weapon.class.getSimpleName());

        assertThat(characters).isNotNull();
        assertThat(rooms).isNotNull();
        assertThat(weapons).isNotNull();

        assertThat(characters.get(0).getBoards().contains(board)).isTrue();
        assertThat(rooms.get(0).getBoards().contains(board)).isTrue();
        assertThat(weapons.get(0).getBoards().contains(board)).isTrue();
    }

    @Test
    void readCardsByBoardsContaining() {
        var cards = this.cardsRepo.readCardsByBoardsContaining(board);

        assertThat(cards).isNotNull();
        assertThat(cards.get(0).getBoards().contains(board)).isTrue();
    }
}