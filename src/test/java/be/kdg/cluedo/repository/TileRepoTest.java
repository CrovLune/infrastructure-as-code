package be.kdg.cluedo.repository;

import be.kdg.cluedo.domain.Board;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("jpa")
@SpringBootTest

@Slf4j
@Transactional
@Rollback
class TileRepoTest {
    private Board board;
    @Autowired
    GamesRepo gamesRepo;
    @Autowired
    TileRepo tileRepo;

    @BeforeEach
    void init() {
        this.board = this.gamesRepo.findAll().get(0).getBoard();
    }


    //* Testing if finding by board and position (x,y) returns a Tile.
    @Test
    void readTileByArea_BoardAndPosition_xAndPosition_y() {
        assertThat(this.tileRepo.readTileByArea_BoardAndPosition_xAndPosition_y(this.board, 1, 2)).isNotNull();
        //* Out of bounds (1,15)
        assertThat(this.tileRepo.readTileByArea_BoardAndPosition_xAndPosition_y(this.board, 1, 15)).isNull();
    }

    //* Testing if finding by Board and Area returns all the tiles in given Area -> 25
    @Test
    void readTilesByArea_BoardAndArea_Name() {
        var area = new ArrayList<>(this.board.getAreas()).get(0);
        assertThat(this.tileRepo.readTilesByArea_BoardAndArea_Name(this.board, area.getName()).size()).isEqualTo(25);
    }

    @Test
    void readTilesByArea_Board() {
        assertThat(this.tileRepo.readTilesByArea_Board(this.board).size()).isEqualTo(225);
    }
}