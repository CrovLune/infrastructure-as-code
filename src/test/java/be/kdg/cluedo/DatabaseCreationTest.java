package be.kdg.cluedo;

import be.kdg.cluedo.domain.Game;
import be.kdg.cluedo.domain.players.Player;
import be.kdg.cluedo.repository.BoardRepo;
import be.kdg.cluedo.services.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@Slf4j
@Transactional
@Rollback(true)
public class DatabaseCreationTest {
    private Game game;
    @Autowired
    BoardRepo boardRepo;
    @Autowired
    GameService gameService;
    @Autowired
    PlayerService playerService;

    @BeforeEach
    void init() {
        this.game = this.gameService.getAllGames().get(0);
    }

    @Test
    void create() {
        //* is being created with automation method
        //? Check Board
        assertThat(this.boardRepo.findAll().get(0)).isNotNull();
        //? Check Game
        assertThat(this.game).isNotNull();
        //? Check Cards in Game
        assertThat(this.game.getCards()).isNotNull();
        //? Check Players
        var players = this.playerService.getPlayersFor(this.game);
        assertThat(players).isNotNull();
        //? Check Player Cards
        assertThat(players.stream().map(Player::getCards).collect(Collectors.toList())).isNotNull();
    }
}
